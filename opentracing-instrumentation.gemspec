# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'opentracing/instrumentation/version'

Gem::Specification.new do |spec|
  spec.name          = 'opentracing-instrumentation'
  spec.version       = OpenTracing::Instrumentation::VERSION
  spec.authors       = ['Fedorenko Dmitrij']
  spec.email         = ['job@fedorenko-d.ru']

  spec.summary       = 'OpenTracing instrumentation for popular ruby libraries'
  spec.description   = 'OpenTracing instrumentation for Bunny, Faraday, Hutch, Mongo, PORO, ' \
    'Rack, Sidekiq, Sinatra, Thrift'
  spec.homepage      = 'https://gitlab.com/c0va23/ruby-opentracing-instrumentation.'
  spec.license       = 'MIT'

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.required_ruby_version = "> #{File.read('.ruby-version').strip}"

  spec.add_runtime_dependency 'json'
  spec.add_runtime_dependency 'opentracing', '~> 0.5.0'

  spec.add_development_dependency 'activesupport', '~> 5.2'
  spec.add_development_dependency 'bson', '~> 4.0'
  spec.add_development_dependency 'bundler', File.read('.BUNDLER_VERSION').strip
  spec.add_development_dependency 'bunny', '~> 2.0'
  spec.add_development_dependency 'faraday', '~> 1.0'
  spec.add_development_dependency 'hutch', '~> 0.26.0'
  spec.add_development_dependency 'rack', '~> 2.2.2'
  spec.add_development_dependency 'rake', '~> 13.0'
  spec.add_development_dependency 'redis', '~> 3.3.5'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'rubocop', '~> 1.15.0'
  spec.add_development_dependency 'rubocop-performance', '~> 1.11.3'
  spec.add_development_dependency 'rubocop-rake', '~> 0.5.1'
  spec.add_development_dependency 'rubocop-rspec', '~> 2.3.0'
  spec.add_development_dependency 'rubocop-thread_safety', '~> 0.4.2'
  spec.add_development_dependency 'thrift', '~> 0.14.1'
end
