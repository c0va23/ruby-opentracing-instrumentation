# Opentracing::Instrumentation

This gem provides instrumentation for some popular gems. Its gem not use
monkey patching, instrumentation provided only as middleware or wrappers.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'opentracing-instrumentation', require: 'opentracing/instrumentation'
```

And then execute:

```bash
bundle
```

Or install it yourself as:

```bash
gem install opentracing-instrumentation
```

## Usage

[Documentation](https://rubydoc.info/gems/opentracing-instrumentation/)

### Gems

#### [bunny](https://github.com/ruby-amqp/bunny)

Documentation [OpenTracing::Instrumentation::Bunny](https://rubydoc.info/gems/opentracing-instrumentation/OpenTracing/Instrumentation/Bunny)

Tested versions: `2.14.3`

#### [faraday](https://github.com/lostisland/faraday)

Documentation: [OpenTracing::Instrumentation::Faraday](https://rubydoc.info/gems/opentracing-instrumentation/OpenTracing/Instrumentation/Faraday)

Tested versions: `0.9.2`, `1.0.1`

#### [hutch](https://github.com/gocardless/hutch)

Documentation: [OpenTracing::Instrumentation::Hutch](https://rubydoc.info/gems/opentracing-instrumentation/OpenTracing/Instrumentation/Hutch)

Tested versions: `0.21`, `0.26`

#### [mongo](https://github.com/mongodb/mongo-ruby-driver)

Documentation: [OpenTracing::Instrumentation::Mongo](https://rubydoc.info/gems/opentracing-instrumentation/OpenTracing/Instrumentation/Mongo)

Tested versions: `2.8`

#### [rack](https://github.com/rack/rack)

Documentation: [OpenTracing::Instrumentation::Rack](https://rubydoc.info/gems/opentracing-instrumentation/OpenTracing/Instrumentation/Rack)

Tested versions: `2.0`, `2.2`

#### [redis](https://github.com/redis/redis-rb)

Documentation: [OpenTracing::Instrumentation::Redis](https://rubydoc.info/gems/opentracing-instrumentation/OpenTracing/Instrumentation/Redis)

Tested versions: `3.3.5`

#### [sidekiq](https://github.com/mperham/sidekiq)

Documentation: [OpenTracing::Instrumentation::Sidekiq](https://rubydoc.info/gems/opentracing-instrumentation/OpenTracing/Instrumentation/Sidekiq)

Tested versions `4.2`

#### [sinatra](https://github.com/sinatra/sinatra)

Documentation: [OpenTracing::Instrumentation::Sinatra](https://rubydoc.info/gems/opentracing-instrumentation/OpenTracing/Instrumentation/Sinatra)

Tested version: `2.0.8`

#### [thrift](https://github.com/apache/thrift/tree/master/lib/rb)

Documentation: [OpenTracing::Instrumentation::Thrift](https://rubydoc.info/gems/opentracing-instrumentation/OpenTracing/Instrumentation/Thrift)

Tested versions: `0.11.0` 

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake` to run the tests and lints. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `GEM_VERSION`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on [GitLab](https://gitlab.com/c0va23/opentracing-instrumentation).

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
