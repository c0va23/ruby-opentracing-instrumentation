# frozen_string_literal: true

require 'spec_helper'

RSpec.describe OpenTracing::Instrumentation do
  it 'has a version number' do
    expect(OpenTracing::Instrumentation::VERSION).not_to be nil
  end
end
