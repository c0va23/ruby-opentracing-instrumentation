# frozen_string_literal: true

require 'spec_helper'
require 'opentracing/instrumentation/object_wrapper'

RSpec.describe OpenTracing::Instrumentation::ObjectWrapper do
  before do
    test_class = Class.new do
      def initialize(target_proc)
        @target_proc = target_proc
      end

      def target
        @target_proc.call
      end
    end
    stub_const('TestObject', test_class)
  end

  let(:tracer) { Test::Tracer.new }
  let(:source_test_object) { TestObject.new(target_proc) }
  let(:wrapped_test_object) do
    described_class.new(source_test_object, tracer: tracer)
  end

  context 'when target return result' do
    let(:expected_result) { rand }
    let(:target_proc) { -> { expected_result } }

    describe 'method call span' do
      before { wrapped_test_object.target }

      it 'create span' do
        expect(tracer).to have_span('object_call(TestObject#target)')
          .finished.with_tags(
            'object.class' => 'TestObject',
            'object.method' => 'target',
          )
      end
    end

    describe 'method result' do
      subject(:result) { wrapped_test_object.target }

      it 'return original method result' do
        expect(result).to eq expected_result
      end
    end
  end

  context 'when taret raise error' do
    let(:expected_exception) { StandardError.new('custom error') }
    let(:target_proc) { -> { raise expected_exception } }

    describe 'method call' do
      before do
        wrapped_test_object.target rescue nil
      end

      it 'create span with error' do
        expect(tracer).to have_span('object_call(TestObject#target)').finished.with_tags(
          'object.class' => 'TestObject',
          'object.method' => 'target',
          'error' => true,
        )
      end
    end

    it 'reraise error' do
      expect { wrapped_test_object.target }.to raise_error(expected_exception)
    end
  end

  describe '#respond_to_missing?' do
    subject(:respond_to_missing) do
      wrapped_test_object.send(:respond_to_missing?, :target)
    end

    let(:target_proc) { 'ok' }

    it 'return true if method exists' do
      expect(respond_to_missing).to be true
    end
  end
end
