# frozen_string_literal: true

require 'spec_helper'

RSpec.describe OpenTracing::Instrumentation::Thrift::TracedProtocolOperationNameBuilder do
  let(:operation_name_builder) { described_class.new }

  describe '#build_operation_name' do
    subject(:operation_name) do
      operation_name_builder.build_operation_name(direction, name, type)
    end

    let(:direction) { 'write' }
    let(:name) { 'method_name' }
    let(:type) { ::Thrift::MessageTypes::CALL }

    it 'return formated operation name' do
      expected_operation_name = \
        'thrift(direction=write, name=method_name, type=CALL)'

      expect(operation_name).to eq(expected_operation_name)
    end
  end
end
