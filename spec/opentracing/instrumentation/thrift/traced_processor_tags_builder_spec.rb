# frozen_string_literal: true

require 'spec_helper'

RSpec.describe OpenTracing::Instrumentation::Thrift::TracedProcessorTagsBuilder do
  let(:tags_builder) { described_class.new }
  let(:protocol_tags_builder) do
    OpenTracing::Instrumentation::Thrift::TracedProtocolTagsBuilder.new
  end

  describe '#build_tags' do
    subject(:tags) { tags_builder.build_tags(protocol, name, type) }

    let(:transport) { instance_double('Transport') }
    let(:protocol) { instance_double('Protocol', trans: transport) }
    let(:name) { 'Service:method' }
    let(:type) { ::Thrift::MessageTypes::CALL }

    it 'return static tags' do
      expect(tags).to include described_class::DEFAULT_STATIC_TAGS
    end

    it 'return protocol tags' do
      expect(tags).to include protocol_tags_builder.build_protocol_tags(protocol)
    end

    it 'return message tags' do
      expect(tags).to include protocol_tags_builder.build_message_tags(name, type)
    end
  end
end
