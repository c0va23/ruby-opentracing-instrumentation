# frozen_string_literal: true

require 'spec_helper'

RSpec.describe OpenTracing::Instrumentation::Thrift::TracedProtocolFactory do
  let(:transport) { ::Thrift::MemoryBufferTransport.new }
  let(:upstream_protocol_factory) { ::Thrift::BinaryProtocolFactory.new }
  let(:traced_protocol_factory) do
    described_class.new(upstream_protocol_factory)
  end

  describe '#get_protocol' do
    subject(:get_protocol) { traced_protocol_factory.get_protocol(transport) }

    it 'return instance of TracedProtocol' do
      expect(get_protocol).to \
        be_instance_of(OpenTracing::Instrumentation::Thrift::TracedProtocol)
    end
  end
end
