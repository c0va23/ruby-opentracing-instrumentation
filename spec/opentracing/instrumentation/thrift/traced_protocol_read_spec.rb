# frozen_string_literal: true

require 'spec_helper'

RSpec.describe OpenTracing::Instrumentation::Thrift::TracedProtocol do
  let(:transport) { Thrift::MemoryBufferTransport.new }
  let(:upstream_protocol) { Thrift::BinaryProtocol.new(transport) }
  let(:tracer) { Test::Tracer.new }
  let(:operation_name_builder) do
    OpenTracing::Instrumentation::Thrift::\
      TracedProtocolOperationNameBuilder.new(
        operation_name_pattern: 'thrift_read',
      )
  end
  let(:config) do
    OpenTracing::Instrumentation::Thrift::TracedProtocolConfig.new(
      tracer: tracer,
      operation_name_builder: operation_name_builder,
    )
  end
  let(:traced_protocol) do
    described_class.new(upstream_protocol, config: config)
  end
  let(:method_name) { 'target_method' }
  let(:name) { method_name }
  let(:type) { Thrift::MessageTypes::CALL }
  let(:seqid) { rand(1000) }
  let(:expected_result) { [name, type, seqid] }

  before do
    allow(upstream_protocol).to receive(:read_message_begin)
      .and_return(expected_result)
  end

  describe '#read_message_begin' do
    subject(:read_message_begin) { traced_protocol.read_message_begin }

    it 'return upstream result' do
      expect(read_message_begin).to eq(expected_result)
    end

    it 'call upstream write_message_begin' do
      read_message_begin
      expect(upstream_protocol).to have_received(:read_message_begin)
    end

    it 'create in progess span with protocol tag' do
      read_message_begin
      expect(tracer).to have_span('thrift_read').in_progress.with_tags(
        'thrift.protocol' => upstream_protocol.class.to_s,
      )
    end

    context 'when transport not wrapped' do
      let(:transport) { Thrift::MemoryBufferTransport.new }

      it 'create in progess span with protocol tag' do
        read_message_begin
        expect(tracer).to have_span('thrift_read').in_progress.with_tags(
          'thrift.transport' => transport.class.to_s,
        )
      end
    end

    context 'when transport wrapped' do
      let(:inner_transport) { Thrift::MemoryBufferTransport.new }
      let(:transport) { Thrift::BufferedTransport.new(inner_transport) }

      it 'create in progess span with protocol tag' do
        read_message_begin
        expect(tracer).to have_span('thrift_read').in_progress.with_tags(
          'thrift.transport' => "#{transport.class}(#{inner_transport.class})",
        )
      end
    end

    context 'when method not have service prefix' do
      let(:name) { method_name }

      it 'create not finished span without service and with method' do
        read_message_begin
        expect(tracer).to have_span('thrift_read').in_progress.with_tags(
          'thrift.method' => method_name,
          'thrift.multiplexed' => false,
        )
      end
    end

    context 'when method have service prefix' do
      let(:service_name) { 'TestService' }
      let(:name) { "#{service_name}:#{method_name}" }

      it 'create not finished span with method tags' do
        read_message_begin
        expect(tracer).to have_span('thrift_read').in_progress.with_tags(
          'thrift.method' => method_name,
        )
      end

      it 'create span with service tags' do
        read_message_begin
        expect(tracer).to have_span('thrift_read').with_tags(
          'thrift.service_name' => service_name,
          'thrift.multiplexed' => true,
        )
      end
    end

    context 'when type is REPLY' do
      let(:type) { Thrift::MessageTypes::REPLY }

      it 'create not finished span with type call' do
        read_message_begin
        expect(tracer).to have_span('thrift_read').in_progress.with_tags(
          'thrift.type' => 'REPLY',
        )
      end
    end

    context 'when type is EXCEPTION' do
      let(:type) { Thrift::MessageTypes::EXCEPTION }

      it 'create not finished span with type call' do
        read_message_begin
        expect(tracer).to have_span('thrift_read').in_progress.with_tags(
          'thrift.type' => 'EXCEPTION',
        )
      end
    end

    context 'when config overide read_operation_name' do
      let(:read_operation_name) { 'thrift_request_read' }
      let(:operation_name_builder) do
        OpenTracing::Instrumentation::Thrift::\
          TracedProtocolOperationNameBuilder.new(
            operation_name_pattern: read_operation_name,
          )
      end
      let(:config) do
        OpenTracing::Instrumentation::Thrift::TracedProtocolConfig.new(
          tracer: tracer,
          operation_name_builder: operation_name_builder,
        )
      end

      it 'create span with overided name' do
        read_message_begin
        expect(tracer).to have_span(read_operation_name).in_progress
      end
    end

    context 'when protocl raise error' do
      let(:read_exception) { StandardError.new('read error') }

      before do
        allow(upstream_protocol).to receive(:read_message_begin)
          .and_raise(read_exception)
      end

      it 'raise error' do
        expect { read_message_begin }.to raise_error(read_exception)
      end

      it 'not create span' do
        read_message_begin rescue nil
        expect(tracer).not_to have_span('thrift_read')
      end
    end
  end

  describe '#read_message_end' do
    subject(:read_message_end) { traced_protocol.read_message_end }

    context 'when read message begin and end is success' do
      before do
        traced_protocol.read_message_begin
        allow(upstream_protocol).to receive(:read_message_end)
      end

      it 'call upstream write_message_end' do
        read_message_end
        expect(upstream_protocol).to have_received(:read_message_end)
      end

      it 'create finished span' do
        read_message_end
        expect(tracer).to have_span('thrift_read').finished
      end
    end

    context 'when read message is success, but end raise error' do
      before do
        allow(traced_protocol).to receive(:read_message_begin).and_raise
        traced_protocol.read_message_begin rescue nil
        allow(upstream_protocol).to receive(:read_message_end)
      end

      it 'not raise error' do
        expect { read_message_end }.not_to raise_error
      end

      it 'not create span' do
        read_message_end rescue nil
        expect(tracer).not_to have_span('thrift_read')
      end
    end
  end
end
