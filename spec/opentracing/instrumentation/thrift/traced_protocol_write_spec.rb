# frozen_string_literal: true

require 'spec_helper'

RSpec.describe OpenTracing::Instrumentation::Thrift::TracedProtocol do
  let(:transport) { Thrift::MemoryBufferTransport.new }
  let(:upstream_protocol) { Thrift::BinaryProtocol.new(transport) }
  let(:tracer) { Test::Tracer.new }
  let(:operation_name_builder) do
    OpenTracing::Instrumentation::Thrift::\
      TracedProtocolOperationNameBuilder.new(
        operation_name_pattern: 'thrift_write',
      )
  end
  let(:config) do
    OpenTracing::Instrumentation::Thrift::TracedProtocolConfig.new(
      tracer: tracer,
      operation_name_builder: operation_name_builder,
    )
  end
  let(:traced_protocol) do
    described_class.new(upstream_protocol, config: config)
  end
  let(:method_name) { 'target_method' }
  let(:name) { method_name }
  let(:type) { Thrift::MessageTypes::CALL }
  let(:seqid) { rand(1000) }

  before do
    allow(upstream_protocol).to receive(:write_message_begin)
  end

  describe '#write_message_begin' do
    subject(:write_message_begin) do
      traced_protocol.write_message_begin(name, type, seqid)
    end

    it 'call upstream write_message_begin' do
      write_message_begin
      expect(upstream_protocol).to have_received(:write_message_begin)
    end

    it 'create in progess span with protocol tag' do
      write_message_begin
      expect(tracer).to have_span('thrift_write').in_progress.with_tags(
        'thrift.protocol' => upstream_protocol.class.to_s,
      )
    end

    context 'when transport not wrapped' do
      let(:transport) { Thrift::MemoryBufferTransport.new }

      it 'create in progess span with protocol tag' do
        write_message_begin
        expect(tracer).to have_span('thrift_write').in_progress.with_tags(
          'thrift.transport' => transport.class.to_s,
        )
      end
    end

    context 'when transport wrapped' do
      let(:inner_transport) { Thrift::MemoryBufferTransport.new }
      let(:transport) { Thrift::BufferedTransport.new(inner_transport) }

      it 'create in progess span with protocol tag' do
        write_message_begin
        expect(tracer).to have_span('thrift_write').in_progress.with_tags(
          'thrift.transport' => "#{transport.class}(#{inner_transport.class})",
        )
      end
    end

    context 'when method not have service prefix' do
      let(:name) { method_name }

      it 'create not finished span without service and with method' do
        write_message_begin
        expect(tracer).to have_span('thrift_write').in_progress.with_tags(
          'thrift.method' => method_name,
          'thrift.multiplexed' => false,
        )
      end
    end

    context 'when method have service prefix' do
      let(:service_name) { 'TestService' }
      let(:name) { "#{service_name}:#{method_name}" }

      it 'create not finished span with method name' do
        write_message_begin
        expect(tracer).to have_span('thrift_write').in_progress.with_tags(
          'thrift.method' => method_name,
        )
      end

      it 'create span with service name' do
        write_message_begin
        expect(tracer).to have_span('thrift_write').in_progress.with_tags(
          'thrift.service_name' => service_name,
          'thrift.multiplexed' => true,
        )
      end
    end

    context 'when type is call' do
      let(:type) { Thrift::MessageTypes::CALL }

      it 'create not finished span with type call' do
        write_message_begin
        expect(tracer).to have_span('thrift_write').in_progress.with_tags(
          'thrift.type' => 'CALL',
        )
      end
    end

    context 'when type is repty' do
      let(:type) { Thrift::MessageTypes::ONEWAY }

      it 'create not finished span with type call' do
        write_message_begin
        expect(tracer).to have_span('thrift_write').in_progress.with_tags(
          'thrift.type' => 'ONEWAY',
        )
      end
    end

    context 'when config overide write_operation_name' do
      let(:write_operation_name) { 'thrift_request_write' }
      let(:operation_name_builder) do
        OpenTracing::Instrumentation::Thrift::\
          TracedProtocolOperationNameBuilder.new(
            operation_name_pattern: 'thrift_request_write',
          )
      end
      let(:config) do
        OpenTracing::Instrumentation::Thrift::TracedProtocolConfig.new(
          tracer: tracer,
          operation_name_builder: operation_name_builder,
        )
      end

      it 'create span with overided name' do
        write_message_begin
        expect(tracer).to have_span(write_operation_name).in_progress
      end
    end

    context 'when upstream protocol raise error' do
      before do
        allow(upstream_protocol).to receive(:write_message_begin)
          .and_raise(write_exception)
      end

      let(:write_exception) { StandardError.new('write error') }

      it 'raise error' do
        expect { write_message_begin }.to raise_error(write_exception)
      end

      it 'create finished span with error tags' do
        write_message_begin rescue nil
        expect(tracer).to have_span('thrift_write')
          .finished.with_tags('error' => true)
      end
    end
  end

  describe '#write_message_end' do
    subject(:write_message_end) { traced_protocol.write_message_end }

    context 'when  write_message_end is success' do
      before do
        traced_protocol.write_message_begin(name, type, seqid)
        allow(upstream_protocol).to receive(:write_message_end)
      end

      it 'call upstream write_message_end' do
        write_message_end
        expect(upstream_protocol).to have_received(:write_message_end)
      end

      it 'create finished span' do
        write_message_end
        expect(tracer).to have_span('thrift_write').finished
      end
    end

    context 'when write_message_end raise error' do
      let(:write_exception) { StandardError.new('write eror') }

      before do
        traced_protocol.write_message_begin(name, type, seqid)
        allow(upstream_protocol).to receive(:write_message_end)
          .and_raise(write_exception)
      end

      it 'raise error' do
        expect { write_message_end }.to raise_error(write_exception)
      end

      it 'call upstream write_message_end' do
        write_message_end rescue nil
        expect(upstream_protocol).to have_received(:write_message_end)
      end

      it 'create finished span' do
        write_message_end rescue nil
        expect(tracer).to have_span('thrift_write').finished
      end
    end

    context 'when write_message_begin raise error' do
      let(:write_exception) { StandardError.new('write eror') }

      before do
        allow(upstream_protocol).to receive(:write_message_begin)
          .and_raise(write_exception)
        traced_protocol.write_message_begin(name, type, seqid) rescue nil
        allow(upstream_protocol).to receive(:write_message_end)
      end

      it 'not raise erroro' do
        expect { write_message_end }.not_to raise_error
      end
    end
  end
end
