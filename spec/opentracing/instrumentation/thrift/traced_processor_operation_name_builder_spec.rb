# frozen_string_literal: true

require 'spec_helper'

RSpec.describe OpenTracing::Instrumentation::Thrift::TracedProcessorOperationNameBuilder do
  describe '#build' do
    subject(:operation_name) { builder.build(name, type, seq_id) }

    let(:name) { 'Service:method' }
    let(:type) { ::Thrift::MessageTypes::CALL }
    let(:seq_id) { rand(1_000) }

    context 'with deafult template' do
      let(:builder) { described_class.new }

      it 'create defult operation name' do
        expect(operation_name).to eq "thrift_processor(#{name})"
      end
    end

    context 'with custom template' do
      let(:operation_name_template) { '(%<name>s, %<type>s)' }
      let(:builder) do
        described_class.new(operation_name_template: operation_name_template)
      end

      it 'create defult operation name' do
        expect(operation_name).to eq "(#{name}, CALL)"
      end
    end
  end
end
