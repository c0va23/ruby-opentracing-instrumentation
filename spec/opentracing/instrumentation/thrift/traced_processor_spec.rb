# frozen_string_literal: true

require 'spec_helper'

RSpec.describe OpenTracing::Instrumentation::Thrift::TracedProcessor do
  let(:tracer) { Test::Tracer.new }
  let(:source_processor) { instance_double('Thrift::Processor', process: nil) }
  let(:tags_builder) { instance_double('TagsBulder') }
  let(:operation_name_builder) { instance_double('OperationNameBuilder') }
  let(:processor) do
    described_class.new(source_processor) do |config|
      config.tracer = tracer
      config.trace_protocol = trace_protocol
      config.operation_name_builder = operation_name_builder
      config.tags_builder = tags_builder
    end
  end

  describe '#process' do
    subject(:process) { processor.process(iproto, oproto) }

    let(:message_name) { 'Service;method' }
    let(:message_type) { ::Thrift::MessageTypes::CALL }
    let(:message_seq_id) { rand(1000) }
    let(:message_begin) { [message_name, message_type, message_seq_id] }
    let(:transport) { instance_double('Thrift::Transport') }
    let(:iproto) do
      instance_double(
        'IProtocol',
        read_message_begin: message_begin,
        trans: transport,
      )
    end
    let(:cached_iproto) { described_class::ReadCachedProtocol.new(iproto) }
    let(:oproto) { instance_double('IProtocol', trans: transport) }
    let(:operation_name) { 'thrift_processor' }
    let(:trace_protocol) { false }
    let(:tags) { { 'tag' => 1 } }

    before do
      allow(tags_builder).to receive(:build_tags)
        .with(iproto, message_name, message_type)
        .and_return(tags)

      allow(operation_name_builder).to receive(:build)
        .with(message_name, message_type, message_seq_id)
        .and_return(operation_name)
    end

    context 'when trace_protocol is true' do
      let(:trace_protocol) { true }
      let(:expected_iproto) do
        OpenTracing::Instrumentation::Thrift::TracedProtocol.new(cached_iproto) do |c|
          c.tracer = tracer
        end
      end

      let(:expected_oproto) do
        OpenTracing::Instrumentation::Thrift::TracedProtocol.new(oproto) do |c|
          c.tracer = tracer
        end
      end

      it 'create finished span' do
        process
        expect(tracer).to have_span(operation_name).finished
      end

      it 'create span with tags' do
        process
        expect(tracer).to have_span(operation_name).with_tags(tags)
      end

      it 'call source processor with wrapped cached input protocol' do
        process
        expect(source_processor).to have_received(:process).with(expected_iproto, anything)
      end

      it 'call source processor with wrapped output protocol' do
        process
        expect(source_processor).to have_received(:process).with(anything, expected_oproto)
      end
    end

    context 'when trace_protocol is false' do
      let(:trace_protocol) { false }

      it 'create finished span' do
        process
        expect(tracer).to have_span(operation_name).finished
      end

      it 'call source processor with cached protocols' do
        process
        expect(source_processor).to have_received(:process).with(cached_iproto, oproto)
      end
    end

    context 'when process raise error' do
      let(:process_error) { StandardError.new('process error') }

      before do
        allow(source_processor).to receive(:process).and_raise(process_error)
      end

      it 'raise error' do
        expect { process }.to raise_error(process_error)
      end

      it 'create finished span' do
        process rescue nil
        expect(tracer).to have_span(operation_name).finished
      end

      it 'create span with error tag' do
        process rescue nil
        expect(tracer).to have_span(operation_name).with_tags('error' => true)
      end
    end

    context 'when build span raise error' do
      let(:span_error) { StandardError.new('span error') }

      before do
        allow(tags_builder).to receive(:build_tags).and_raise(span_error)
      end

      it 'not raise error' do
        expect { process }.not_to raise_error
      end

      it 'not create span' do
        process
        expect(tracer).not_to have_span(operation_name)
      end

      it 'call process' do
        process
        expect(source_processor).to have_received(:process)
      end
    end
  end
end
