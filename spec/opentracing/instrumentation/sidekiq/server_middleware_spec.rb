# frozen_string_literal: true

require 'spec_helper'

RSpec.describe OpenTracing::Instrumentation::Sidekiq::ServerMiddleware do
  let(:tracer) { Test::Tracer.new }
  let(:tagger) do
    instance_double('JobTagger')
  end
  let(:middleware_args) { { tracer: tracer, tagger: tagger } }
  let(:client_middleware) { described_class.new(**middleware_args) }

  describe '#call' do
    subject(:call) do
      client_middleware.call(worker, job, queue_name) do
        block.call
      end
    end

    before do
      allow(tagger).to receive(:build_tags).with(job, 'consumer').and_return(tags)
      allow(tagger).to receive(:write_args_log).with(
        kind_of(OpenTracing::Span),
        jid,
        args,
      )
    end

    let(:block) { instance_double('block', call: nil) }
    let(:queue_name) { 'simple_queue' }
    let(:worker) { instance_double('SimpleWorker') }
    let(:worker_class) { 'SimpleWorker' }
    let(:retry_) { true }
    let(:redis_pool) { instance_double('RedisPool') }
    let(:jid) { 'jid' }
    let(:args) { [1, 2, 3] }
    let(:job) do
      {
        'class' => worker_class,
        'queue' => queue_name,
        'retry' => retry_,
        'jid' => jid,
        'args' => args,
      }
    end
    let(:tags) { { 'sidekiq.class': worker_class } }
    let(:operation_name) { 'sidekiq_perform(SimpleWorker)' }

    context 'when call not raise error' do
      before { call }

      it 'create span with tags' do
        expect(tracer).to have_span(operation_name).finished.with_tags(tags)
      end

      it 'call block' do
        expect(block).to have_received(:call)
      end
    end

    context 'when call raise error' do
      before do
        allow(block).to receive(:call).and_raise(exception)
      end

      let(:exception) { StandardError.new('simple error') }

      it 'create finished span with error tag' do
        call rescue nil
        expect(tracer).to have_span(operation_name).finished.with_tags(
          'error' => true,
        )
      end

      it 'reraise error' do
        expect { call }.to raise_error(exception)
      end
    end
  end
end
