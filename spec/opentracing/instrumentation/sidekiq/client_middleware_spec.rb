# frozen_string_literal: true

require 'spec_helper'

RSpec.describe OpenTracing::Instrumentation::Sidekiq::ClientMiddleware do
  let(:tracer) { Test::Tracer.new }
  let(:tagger) do
    instance_double('JobTagger')
  end
  let(:middleware_args) { { tracer: tracer, tagger: tagger } }
  let(:client_middleware) { described_class.new(**middleware_args) }

  describe '#call' do
    subject(:call) do
      client_middleware.call(worker_class, job, queue_name, redis_pool) do
        block.call
      end
    end

    around do |spec|
      parent_scope
      spec.call
      parent_scope.close
    end

    let(:parent_scope) { tracer.start_active_span('parent') }
    let(:block) { instance_double('block', call: nil) }
    let(:queue_name) { 'simple_queue' }
    let(:worker_class) { 'SimpleWorker' }
    let(:retry_) { true }
    let(:redis_pool) { instance_double('RedisPool') }
    let(:args) { [1, 2, 3] }
    let(:jid) { 'jid' }
    let(:job) do
      {
        'class' => worker_class,
        'queue' => queue_name,
        'retry' => retry_,
        'jid' => jid,
        'args' => args,
      }
    end
    let(:tags) { { 'sidekiq.class': worker_class } }
    let(:operation_name) { 'sidekiq_enqueue(SimpleWorker)' }

    before do
      allow(tagger).to receive(:build_tags).with(job, 'producer').and_return(tags)
      allow(tagger).to receive(:write_args_log).with(
        kind_of(OpenTracing::Span),
        jid,
        args,
      )
    end

    context 'when call not raise error' do
      it 'create span with common tags' do
        call
        expect(tracer).to have_span(operation_name).finished.with_tags(tags)
      end

      it 'call block' do
        call
        expect(block).to have_received(:call)
      end

      it 'inject trace id into job' do
        expect { call }
          .to change { job['trace_id'] }.to(parent_scope.span.context.trace_id)
      end
    end

    context 'when call raise error' do
      before do
        allow(block).to receive(:call).and_raise(exception)
      end

      let(:exception) { StandardError.new('simple error') }

      it 'create finished span with error tag' do
        call rescue nil
        expect(tracer).to have_span(operation_name).finished.with_tags(
          'error' => true,
        )
      end

      it 'reraise error' do
        expect { call }.to raise_error(exception)
      end
    end
  end
end
