# frozen_string_literal: true

require 'spec_helper'

RSpec.describe OpenTracing::Instrumentation::Sidekiq::JobTagger do
  let(:builder) { described_class.new(log_args: log_args) }
  let(:log_args) { true }

  describe '#build_tags' do
    subject(:tags) { builder.build_tags(job, span_kind) }

    let(:span_kind) { 'server' }
    let(:class_name) { 'SampleJob' }
    let(:queue) { 'main_queue' }
    let(:retry_) { nil }
    let(:job) do
      {
        'class' => class_name,
        'queue' => queue,
        'retry' => retry_,
      }
    end

    it 'return default opentracing tags' do
      expect(tags).to include(
        component: 'sidekiq',
        'span.kind': span_kind,
      )
    end

    it 'return sidekiq tags' do
      expect(tags).to include(
        'sidekiq.queue': queue,
        'sidekiq.class': class_name,
        'sidekiq.retry': retry_,
      )
    end
  end
end
