# frozen_string_literal: true

require 'spec_helper'
require 'redis'
require_relative './redis_mock_connection'

RSpec.describe OpenTracing::Instrumentation::Redis::TracingDriverWrapper do
  let(:tracer) { Test::Tracer.new }
  let(:config) do
    OpenTracing::Instrumentation::Redis::Config.new do |config|
      config.tracer = tracer
      config.log_args = true
      config.log_reply = true
    end
  end
  let(:span_builder) do
    OpenTracing::Instrumentation::Redis::SpanBuilder.new(config: config)
  end
  let(:redis_mock_config) { {} }
  let(:host) { '127.0.0.1' }
  let(:port) { 6379 }
  let(:redis_config) do
    {
      driver: described_class,
      background_driver: RedisMockConnection,
      span_builder: span_builder,
      redis_mock_config: redis_mock_config,
      host: host,
      port: port,
    }
  end
  let(:redis_client) { ::Redis.new(redis_config) }

  describe 'command' do
    subject(:perform_command) { redis_client.send(command_name, *args) }

    let(:command_name) { :incr }
    let(:args) { ['key'] }
    let(:expected_operation_name) { "redis(command=#{command_name})" }

    context 'when command success' do
      let(:redis_mock_config) do
        {
          read_callback: -> { [123] },
        }
      end

      before { perform_command }

      it 'create redis_command span with peer tags' do
        expect(tracer).to have_span(expected_operation_name).finished.with_tags(
          'peer.address' => "#{host}:#{port}",
          'peer.service' => 'redis',
        )
      end

      it 'create redis_command span with common tags' do
        expect(tracer).to have_span(expected_operation_name).finished.with_tags(
          'component' => 'kv',
          'span.kind' => 'client',
        )
      end

      it 'create redis_command span with redis tags' do
        expect(tracer).to have_span(expected_operation_name).finished.with_tags(
          'redis.driver' => 'RedisMockConnection',
        )
      end

      it 'create redis command with write log' do
        expect(tracer).to have_span(expected_operation_name).finished.with_log(
          event: 'redis_write',
          'redis.command': command_name,
          'redis.args': JSON.dump(args),
        )
      end

      it 'create redis command with read log' do
        expect(tracer).to have_span(expected_operation_name).finished.with_log(
          event: 'redis_read',
          'redis.reply': JSON.dump([123]),
        )
      end

      it 'create redis_command span without error tag' do
        expect(tracer).not_to have_span(expected_operation_name)
          .finished.with_tags('error' => true)
      end
    end

    context 'when write raise timeout' do
      let(:redis_mock_config) do
        {
          write_callback: ->(_command) { raise ::Redis::TimeoutError },
        }
      end

      it 'raise error' do
        expect { perform_command }.to raise_error(::Redis::TimeoutError)
      end

      it 'create redis_command span with error tags' do
        perform_command rescue nil
        expect(tracer).to have_span(expected_operation_name).finished.with_tags(
          'error' => true,
        )
      end
    end

    context 'when read raise timeout' do
      let(:redis_mock_config) do
        {
          read_callback: -> { raise ::Redis::TimeoutError },
        }
      end

      it 'raise error' do
        expect { perform_command }.to raise_error(::Redis::TimeoutError)
      end

      it 'create redis_command span with error tags' do
        perform_command rescue nil
        expect(tracer).to have_span(expected_operation_name).finished.with_tags(
          'error' => true,
        )
      end
    end
  end

  describe 'pipeline' do
    subject(:perform_pipeline) do
      redis_client.pipelined do |pipeline|
        pipeline.incr('key')
        pipeline.decr('key')
      end
    end

    let(:reply_queue) { [1, 1] }
    let(:redis_mock_config) { { read_callback: -> { reply_queue } } }
    let(:expected_operation_name) { 'redis(command=incr)' }

    before { perform_pipeline }

    it 'create first write command span with write log' do
      expect(tracer).to have_span(expected_operation_name).finished.with_log(
        event: 'redis_write',
        'redis.command': :incr,
        'redis.args': '["key"]',
      )
    end

    it 'create second write command span log' do
      expect(tracer).to have_span(expected_operation_name).finished.with_log(
        event: 'redis_write',
        'redis.command': :decr,
        'redis.args': '["key"]',
      )
    end
  end

  describe 'multi' do
    let(:key) { 'key' }
    let(:perform_multi) do
      redis_client.multi do |multi|
        multi.incr(key)
        multi.decr(key)
      end
    end
    let(:reply_queue) do
      [
        'OK',
        'QUEUE',
        'QUEUE',
        [1, 1],
      ]
    end
    let(:expected_operation_name) { 'redis(command=multi)' }

    before { perform_multi }

    it 'create first write command span with write log' do
      expect(tracer).to have_span(expected_operation_name).finished.with_log(
        event: 'redis_write',
        'redis.command': :incr,
        'redis.args': '["key"]',
      )
    end

    it 'create second write command span log' do
      expect(tracer).to have_span(expected_operation_name).finished.with_log(
        event: 'redis_write',
        'redis.command': :decr,
        'redis.args': '["key"]',
      )
    end
  end
end
