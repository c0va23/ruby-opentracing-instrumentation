# frozen_string_literal: true

class RedisMockConnection
  def self.connect(config)
    redis_mock_config = config.fetch(:redis_mock_config, {})
    new(**redis_mock_config)
  end

  def initialize(write_callback: nil, read_callback: nil)
    @write_callback = write_callback
    @read_callback = read_callback
  end

  def write(command)
    @write_callback&.call(command)
  end

  def read
    @read_callback&.call
  end

  def connected?
    true
  end

  def disconnect; end
end
