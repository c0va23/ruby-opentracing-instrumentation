# frozen_string_literal: true

require 'spec_helper'

RSpec.describe OpenTracing::Instrumentation::Hutch::GlobalPropertiesBuilder do
  let(:injector) { instance_double('Injector') }
  let(:trace_id) { SecureRandom.uuid }
  let(:builder) do
    described_class.new(
      headers_injector: injector,
      global_properties_builder: global_properties_builder,
    )
  end

  describe '#call' do
    subject(:properties) { builder.call }

    let(:injected_headers) do
      { 'trace_id' => trace_id }
    end
    let(:other_headers) { {} }

    before do
      allow(injector).to receive(:inject).with(other_headers) do |headers|
        headers.merge!(injected_headers)
      end
    end

    context 'with empty properties builder' do
      let(:global_properties_builder) do
        described_class::EmptyPropertiesBuilder.new
      end

      it 'return headers with injected tracer_id' do
        expect(properties).to eq(headers: { 'trace_id' => trace_id })
      end
    end

    context 'with custom properties builder' do
      let(:other_properties) { { priority: 5 } }
      let(:global_properties_builder) do
        -> { other_properties }
      end

      it 'return headers with injected trace_id with custom properties' do
        expected_properties = \
          other_properties.merge(headers: injected_headers)
        expect(properties).to eq(expected_properties)
      end
    end

    context 'with custom properties builder with headers' do
      let(:other_headers) { { custom: 123 } }
      let(:other_properties) { { priority: 5, headers: other_headers } }
      let(:global_properties_builder) do
        -> { other_properties }
      end

      it 'return headers merged with trace id' do
        expected_properties = \
          other_properties.merge(headers: injected_headers.merge(other_headers))
        expect(properties).to eq expected_properties
      end
    end
  end
end
