# frozen_string_literal: true

require 'spec_helper'

RSpec.shared_context 'with hutch consume tracer' do
  let(:tracer) { Test::Tracer.new }

  let(:consumer) { instance_double('Consumer') }

  let(:operation_name) { 'hutch_consume' }
  let(:operation_name_builder) { instance_double('OperationNameBuilder') }

  let(:tags) { { 'hutch' => 'tags' } }
  let(:tags_builder) { instance_double('TagsBuilder') }

  let(:headers) { {} }
  let(:properties) { { headers: headers } }
  let(:message) { instance_double('Message', properties: properties) }

  let(:logger) { instance_double('Logger', error: nil) }

  let(:consume_tracer_builder) do
    OpenTracing::Instrumentation::Hutch::ConsumeTracerBuilder.new do |config|
      config.tracer = tracer
      config.operation_name_builder = operation_name_builder
      config.tags_builder = tags_builder
      config.logger = logger
    end
  end
  let(:consume_tracer) { consume_tracer_builder.new(consumer) }
end
