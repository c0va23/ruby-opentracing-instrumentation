# frozen_string_literal: true

require 'spec_helper'

RSpec.describe OpenTracing::Instrumentation::Hutch::ConsumeTagsBuilder do
  let(:bunny_tags) { { 'buuny' => 'tag' } }
  let(:bunny_tags_builder) { instance_double('BunnyConsumeTagsBuilder') }
  let(:builder) do
    described_class.new(
      bunny_consume_tags_builder: bunny_tags_builder,
    )
  end
  let(:consumer_class) { stub_const('OtherConsumer', Class.new) }
  let(:consumer) { consumer_class.new }
  let(:delivery_info) { instance_double('DeliveryInfo') }
  let(:properties) { instance_double('Properties') }
  let(:message) do
    instance_double(
      'Message',
      delivery_info: delivery_info,
      properties: properties,
    )
  end

  before do
    allow(bunny_tags_builder).to receive(:build_tags)
      .with(delivery_info, properties).and_return(bunny_tags)
  end

  describe '#build_tags' do
    let(:tags) { builder.build_tags(consumer, message) }

    it 'return bunny tags' do
      expect(tags).to include(bunny_tags)
    end

    it 'return static tags' do
      expect(tags).to include(described_class::DEFAULT_STATIC_TAGS)
    end

    it 'return hutch tags' do
      expect(tags).to include 'hutch.consumer_class' => 'OtherConsumer'
    end
  end
end
