# frozen_string_literal: true

require 'spec_helper'
require_relative './consume_tracer_shared_contexts'

RSpec.describe OpenTracing::Instrumentation::Hutch::ConsumeTracer do
  include_context 'with hutch consume tracer'

  let(:operation_name_error) { StandardError.new('Name error') }

  before do
    allow(operation_name_builder).to receive(:build_operation_name)
      .with(consumer, message).and_raise(operation_name_error)
  end

  describe '#handle' do
    subject(:handle_result) { consume_tracer.handle(message) }

    context 'when consume.process return result' do
      let(:consume_result) { instance_double('Consume result') }

      before do
        allow(consumer).to receive(:process)
          .with(message).and_return(consume_result)
      end

      it 'return consume result' do
        expect(handle_result).to eq(consume_result)
      end

      it 'not create span' do
        handle_result
        expect(tracer).not_to have_span(operation_name)
      end
    end

    context 'when consume.process raise error' do
      let(:consume_error) { StandardError.new('Consume error') }

      before do
        allow(consumer).to receive(:process)
          .with(message).and_raise(consume_error)
      end

      it 'raise error' do
        expect { handle_result }.to raise_error(consume_error)
      end

      it 'not create span' do
        handle_result rescue nil
        expect(tracer).not_to have_span(operation_name)
      end
    end
  end
end
