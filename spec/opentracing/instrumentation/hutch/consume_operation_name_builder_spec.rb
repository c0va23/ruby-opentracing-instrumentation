# frozen_string_literal: true

require 'spec_helper'

RSpec.describe OpenTracing::Instrumentation::Hutch::ConsumeOperationNameBuilder do
  let(:builder) do
    described_class.new(
      routing_key_sanitazer: routing_key_sanitazer,
      operation_name_pattern: operation_name_pattern,
    )
  end
  let(:operation_name_pattern) do
    described_class::DEFAULT_OPERATION_NAME_PATTERN
  end
  let(:delivery_info) { {} }
  let(:properties) { {} }
  let(:message) do
    instance_double(
      'Message',
      delivery_info: delivery_info,
      properties: properties,
    )
  end
  let(:consumer_class) { stub_const('CustomConsumer', Class.new) }
  let(:consumer) { consumer_class.new }
  let(:routing_key_sanitazer) do
    instance_double('RoutingKeySanitazer')
  end

  describe '#build_operation_name' do
    subject(:operation_name) { builder.build_operation_name(consumer, message) }

    context 'without routing_key' do
      before do
        allow(routing_key_sanitazer).to receive(:sanitaze_routing_key)
      end

      it 'return operation name' do
        expected_command_name = \
          'hutch_consume(consumer_class=CustomConsumer)'
        expect(operation_name).to eq expected_command_name
      end

      it 'not call sanitaze_routing_key' do
        operation_name
        expect(routing_key_sanitazer).not_to have_received(:sanitaze_routing_key)
      end
    end

    context 'with routing_key in operation name' do
      let(:delivery_info) { { routing_key: 'source routing key' } }
      let(:sanitazed_routing_key) { 'sanitazed-key' }
      let(:operation_name_pattern) { 'hutch(%<routing_key>s)' }

      before do
        allow(routing_key_sanitazer).to receive(:sanitaze_routing_key)
          .with(delivery_info[:routing_key]).and_return(sanitazed_routing_key)
      end

      it 'operation name with sanitazed routing_key' do
        expect(operation_name).to eq('hutch(sanitazed-key)')
      end
    end
  end
end
