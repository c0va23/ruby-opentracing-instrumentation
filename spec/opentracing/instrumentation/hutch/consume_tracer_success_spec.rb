# frozen_string_literal: true

require 'spec_helper'
require_relative './consume_tracer_shared_contexts'

RSpec.describe OpenTracing::Instrumentation::Hutch::ConsumeTracer do
  include_context 'with hutch consume tracer'

  before do
    allow(operation_name_builder).to receive(:build_operation_name)
      .with(consumer, message).and_return(operation_name)
    allow(tags_builder).to receive(:build_tags)
      .with(consumer, message).and_return(tags)
  end

  describe '#handle' do
    subject(:handle_result) { consume_tracer.handle(message) }

    context 'when consume.process return result' do
      let(:consume_result) { instance_double('Consume result') }

      before do
        allow(consumer).to receive(:process)
          .with(message).and_return(consume_result)
      end

      it 'return consume result' do
        expect(handle_result).to eq(consume_result)
      end

      it 'create finished span' do
        handle_result
        expect(tracer).to have_span(operation_name).finished.with_tags(tags)
      end
    end

    context 'when consume succes, but headers is nil' do
      subject(:handle_result) { consume_tracer.handle(message) }

      let(:consume_result) { instance_double('Consume result') }

      let(:properties) { super().merge(headers: nil) }

      before do
        allow(consumer).to receive(:process)
          .with(message).and_return(consume_result)
      end

      it 'not log error' do
        handle_result
        expect(logger).not_to have_received(:error)
      end
    end

    context 'when consume.process raise error' do
      let(:consume_error) { StandardError.new('Consume error') }

      before do
        allow(consumer).to receive(:process)
          .with(message).and_raise(consume_error)
      end

      it 'raise error' do
        expect { handle_result }.to raise_error(consume_error)
      end

      it 'create finished span' do
        handle_result rescue nil
        expect(tracer).to have_span(operation_name).finished.with_tags(tags)
      end

      it 'create span with error' do
        handle_result rescue nil
        expect(tracer).to have_span(operation_name)
          .finished.with_tags('error' => true)
      end
    end
  end
end
