# frozen_string_literal: true

require 'faraday'
require 'spec_helper'
require 'opentracing/instrumentation/faraday'

RSpec.describe OpenTracing::Instrumentation::Faraday::TraceMiddleware do
  subject(:response) { faraday_connection.get(path) }

  let(:tracer) { Test::Tracer.new }
  let!(:parent_scope) { tracer.start_active_span('parent') }
  let(:url) { 'http://localhost' }
  let(:path) { '/test' }
  let(:faraday_connection) do
    Faraday.new(url) do |connection|
      connection.use described_class do |config|
        config.tracer = tracer
        config.inject = inject
      end
      connection.adapter :test, faraday_stubs
    end
  end
  let(:faraday_stubs) do
    Faraday::Adapter::Test::Stubs.new do |stubs|
      stubs.get(path) do
        [200, {}, [response_payload]]
      end
    end
  end
  let(:response_status) { 200 }
  let(:response_payload) { 'payload' }

  after do
    faraday_stubs.verify_stubbed_calls
    parent_scope.close
  end

  context 'when inject is true' do
    let(:inject) { true }
    let(:faraday_span) do
      tracer.spans.find { |span| span.operation_name == 'faraday_request' }
    end

    it 'inject span context into headers' do
      expect(response.env.request_headers).to include(
        'X-Trace-Id' => parent_scope.span.context.trace_id,
        'X-Parent-Span-Id' => parent_scope.span.context.span_id,
      )
    end

    it 'inject span_id into headers' do
      expect(response.env.request_headers).to include(
        'X-Span-Id' => faraday_span.context.span_id,
      )
    end
  end

  context 'when inject is false' do
    let(:inject) { false }

    it 'not inject X-Trace-Id into headers' do
      expect(response.env.request_headers).not_to have_key('X-Trace-Id')
    end

    it 'not inject X-Parent-Span-Id into headers' do
      expect(response.env.request_headers).not_to have_key('X-Parent-Span-Id')
    end

    it 'not inject X-Span-Id into headers' do
      expect(response.env.request_headers).not_to have_key('X-Span-Id')
    end
  end
end
