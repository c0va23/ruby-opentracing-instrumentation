# frozen_string_literal: true

require 'faraday'
require 'spec_helper'
require 'opentracing/instrumentation/faraday'

RSpec.describe OpenTracing::Instrumentation::Faraday::TraceMiddleware do
  let(:tracer) { Test::Tracer.new }
  let(:url) { 'http://localhost' }
  let(:path) { '/test' }
  let(:faraday_connection) do
    Faraday.new(url) do |connection|
      connection.use described_class do |config|
        config.tracer = tracer
      end
      connection.adapter :test, faraday_stubs
    end
  end

  context 'when response success' do
    let(:response_status) { 200 }
    let(:response_payload) { 'payload' }
    let(:faraday_stubs) do
      Faraday::Adapter::Test::Stubs.new do |stubs|
        stubs.send(request_method, path) do
          [200, {}, [response_payload]]
        end
      end
    end

    before { faraday_connection.run_request(request_method, path, nil, {}) }

    after { faraday_stubs.verify_stubbed_calls }

    context 'when method get' do
      let(:request_method) { :get }

      it 'create span with faraday tags' do
        expect(tracer).to have_span('faraday_request').finished.with_tags(
          'faraday.adapter' => 'Faraday::Adapter::Test',
          'faraday.parallel' => false,
        )
      end

      it 'create span with http tags' do
        expect(tracer).to have_span('faraday_request').finished.with_tags(
          'http.method' => request_method,
          'http.status_code' => response_status,
          'http.url' => url + path,
        )
      end
    end

    context 'when method post' do
      let(:request_method) { :post }

      it 'create span with http tags' do
        expect(tracer).to have_span('faraday_request').finished.with_tags(
          'http.method' => request_method,
          'http.status_code' => response_status,
          'http.url' => url + path,
        )
      end
    end
  end

  context 'when raise connection failed' do
    let(:request_method) { :get }
    let(:faraday_stubs) do
      Faraday::Adapter::Test::Stubs.new do |stubs|
        stubs.send(request_method, path) do
          raise Faraday::ConnectionFailed, nil
        end
      end
    end

    # rubocop:disable Lint/SuppressedException
    before do
      faraday_connection.run_request(request_method, path, nil, {})
    rescue Faraday::ConnectionFailed
    end
    # rubocop:enable Lint/SuppressedException

    after { faraday_stubs.verify_stubbed_calls }

    it 'create span with error tag' do
      expect(tracer).to have_span('faraday_request').finished.with_tags(
        'error' => true,
      )
    end

    it 'create span with http tags' do
      expect(tracer).to have_span('faraday_request').finished.with_tags(
        'http.method' => request_method,
        'http.url' => url + path,
      )
    end
  end
end
