# frozen_string_literal: true

require 'spec_helper'

RSpec.describe OpenTracing::Instrumentation::Rack::StaticCommandNameBuilder do
  describe '#build_command_name' do
    subject(:command_name) { command_name_builder.build_command_name(env) }

    let(:env) { {} }

    context 'when builder not configured' do
      let(:command_name_builder) { described_class.new }

      it 'return default command name' do
        expect(command_name).to eq('rack')
      end
    end

    context 'when builder configured' do
      let(:expected_command_name) { 'alter_rack' }
      let(:command_name_builder) do
        described_class.new(command_name: expected_command_name)
      end

      it 'return alter rack' do
        expect(command_name).to eq(expected_command_name)
      end
    end
  end
end
