# frozen_string_literal: true

require 'spec_helper'
require 'opentracing/instrumentation/rack'

RSpec.describe OpenTracing::Instrumentation::Rack::TraceMiddleware do
  let(:request_uri) { '/test' }
  let(:env) do
    {
      Rack::REQUEST_METHOD => Rack::GET,
      'REQUEST_URI' => request_uri,
      Rack::REQUEST_PATH => request_uri,
    }
  end
  let(:tracer) { Test::Tracer.new }
  let(:rack_app) { instance_double('RackApp') }
  let(:trace_middleware) { described_class.new(rack_app, tracer: tracer) }

  describe '#call' do
    subject(:trace_middleware_response) do
      trace_middleware.call(env)
    end

    context 'when app return valid result' do
      let(:expected_status) { 200 }
      let(:expected_headers) { { 'Content-Type' => 'text/plain' } }
      let(:expected_body) { [] }
      let(:rack_app_response) do
        [expected_status, expected_headers, expected_body]
      end

      before do
        allow(rack_app).to receive(:call).and_return(rack_app_response)
      end

      it { expect(trace_middleware_response).to eq rack_app_response }

      it 'call rack app' do
        trace_middleware_response
        expect(rack_app).to have_received(:call)
      end

      it 'create span' do
        trace_middleware_response
        expect(tracer).to have_span('rack').with_tags('span.kind' => 'server')
      end

      it 'span have request tags' do
        trace_middleware_response
        expect(tracer).to have_span('rack')
          .with_tags('http.method' => Rack::GET, 'http.url' => request_uri)
      end

      it 'span have response tags' do
        trace_middleware_response
        expect(tracer).to have_span('rack')
          .with_tags('http.status' => expected_status)
      end
    end

    context 'when app raise error' do
      let(:exception) { StandardError.new('App error') }

      before do
        allow(rack_app).to receive(:call).and_raise(exception)
      end

      it 'raise error' do
        expect { trace_middleware_response }.to raise_error(exception)
      end

      it 'create span with error tag' do
        begin
          trace_middleware_response
        rescue StandardError # rubocop:disable Lint/SuppressedException
        end
        expect(tracer).to have_span('rack').with_tags('error' => true)
      end
    end
  end
end
