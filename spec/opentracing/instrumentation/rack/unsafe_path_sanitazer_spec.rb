# frozen_string_literal: true

require 'spec_helper'

RSpec.describe OpenTracing::Instrumentation::Rack::UnsafePathSanitazer do
  let(:path_sanitazer) { described_class.new }

  describe '#sanitaze_path' do
    it 'return original path' do
      path = '/orders/1234567890'
      expect(path_sanitazer.sanitaze_path(path)).to eq(path)
    end
  end
end
