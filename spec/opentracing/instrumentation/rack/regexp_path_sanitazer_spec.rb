# frozen_string_literal: true

require 'spec_helper'

RSpec.describe OpenTracing::Instrumentation::Rack::RegexpPathSanitazer do
  let(:path_sanitazer) do
    described_class.new(
      replace_map: described_class::DEFAULT_REPLACE_MAP,
    )
  end

  describe '#sanitaze_path' do
    it 'replace one object_id on path end' do
      path = '/orders/1234567890abcdef12345678'
      expected_path = '/orders/:object_id'
      expect(path_sanitazer.sanitaze_path(path)).to eq(expected_path)
    end

    it 'replace one object_id on path middle' do
      path = '/orders/1234567890abcdef12345678/info'
      expected_path = '/orders/:object_id/info'
      expect(path_sanitazer.sanitaze_path(path)).to eq(expected_path)
    end

    it 'replace multiple object_id into path' do
      path = '/cities/1234567890abcdef12345678/orders/1234567890abcdef12345678'
      expected_path = '/cities/:object_id/orders/:object_id'
      expect(path_sanitazer.sanitaze_path(path)).to eq(expected_path)
    end

    it 'replace one sequence_id on path end' do
      path = '/orders/1234567890'
      expected_path = '/orders/:sequence_id'
      expect(path_sanitazer.sanitaze_path(path)).to eq(expected_path)
    end

    it 'replace one sequence_id on path middle' do
      path = '/orders/1234567890/info'
      expected_path = '/orders/:sequence_id/info'
      expect(path_sanitazer.sanitaze_path(path)).to eq(expected_path)
    end

    it 'replace multiple sequence_id into path' do
      path = '/cities/1234567890/orders/1234567890/info'
      expected_path = '/cities/:sequence_id/orders/:sequence_id/info'
      expect(path_sanitazer.sanitaze_path(path)).to eq(expected_path)
    end

    it 'replace phone into path' do
      path = '/users/+1234567890/info'
      expected_path = '/users/:phone/info'
      expect(path_sanitazer.sanitaze_path(path)).to eq(expected_path)
    end
  end
end
