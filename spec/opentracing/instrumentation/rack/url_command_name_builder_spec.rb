# frozen_string_literal: true

require 'spec_helper'

RSpec.describe OpenTracing::Instrumentation::Rack::UrlCommandNameBuilder do
  describe '#build_command_name' do
    subject(:command_name) do
      command_name_builder.build_command_name(env)
    end

    let(:path) { '/orders/12345' }
    let(:env) do
      {
        Rack::REQUEST_METHOD => Rack::GET,
        Rack::RACK_URL_SCHEME => 'http',
        Rack::HTTP_HOST => 'example.org',
        Rack::REQUEST_PATH => path,
      }
    end
    let(:path_sanitazer) do
      OpenTracing::Instrumentation::Rack::RegexpPathSanitazer.new
    end
    let(:host_sanitazer) do
      OpenTracing::Instrumentation::Rack::RegexpHostSanitazer.new
    end
    let(:command_name_builder) do
      described_class.new(
        path_sanitazer: path_sanitazer,
        host_sanitazer: host_sanitazer,
      )
    end

    it 'return formated command name' do
      expected_command_name = \
        'rack(GET http://example.org/orders/:sequence_id)'
      expect(command_name).to eq(expected_command_name)
    end

    context 'when host have ip and port' do
      let(:env) { super().merge(Rack::HTTP_HOST => '172.0.0.2:12356') }

      it 'return sanitazed host into command name' do
        expected_command_name = \
          'rack(GET http://172.0.0.0/orders/:sequence_id)'
        expect(command_name).to eq(expected_command_name)
      end
    end
  end
end
