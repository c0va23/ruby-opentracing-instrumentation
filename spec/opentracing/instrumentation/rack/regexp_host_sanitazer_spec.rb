# frozen_string_literal: true

require 'spec_helper'

RSpec.describe OpenTracing::Instrumentation::Rack::RegexpHostSanitazer do
  let(:sanitazer) { described_class.new }

  describe '#sanitaze_host' do
    it 'replace ip with mask' do
      expect(sanitazer.sanitaze_host('10.0.1.23:8080')).to eq '10.0.0.0'
    end
  end
end
