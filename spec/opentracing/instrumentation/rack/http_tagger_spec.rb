# frozen_string_literal: true

require 'spec_helper'
require 'opentracing/instrumentation/rack'

RSpec.describe OpenTracing::Instrumentation::Rack::HttpTagger do
  let(:http_tagger) { described_class.new }

  describe '#request_tags' do
    subject(:request_tags) { http_tagger.request_tags(env) }

    let(:env) do
      {
        'HTTP_CONTENT_TYPE' => 'text/plain',
        'HTTP_CONNECTION' => 'keep-alive',
        'HTTP_USER_AGENT' => 'test/1.0',
      }
    end

    it 'return valid tags' do
      expect(request_tags).to include(
        'http.request.content_type' => 'text/plain',
        'http.request.connection' => 'keep-alive',
        'http.request.user_agent' => 'test/1.0',
      )
    end
  end

  describe '#response_tags' do
    subject(:response_tags) { http_tagger.response_tags(headers) }

    context 'when headers in normal form' do
      let(:headers) do
        {
          'Content-Type' => 'text/plain',
          'Connection' => 'keep-alive',
          'Keep-Alive' => 'timeout=60',
        }
      end

      it 'return valid tags' do
        expect(response_tags).to include(
          'http.response.content_type' => 'text/plain',
          'http.response.connection' => 'keep-alive',
          'http.response.keep_alive' => 'timeout=60',
        )
      end
    end
  end
end
