# frozen_string_literal: true

require 'spec_helper'

RSpec.describe OpenTracing::Instrumentation::Rack::ExtractMiddleware do
  let(:request_uri) { '/test' }
  let(:tracer) { Test::Tracer.new }
  let(:rack_app) do
    lambda do |_env|
      tracer.start_active_span('inner_span').close
    end
  end

  describe '#call' do
    let(:app) { described_class.new(rack_app, tracer: tracer) }
    let(:headers) { Rack::Utils::HeaderHash.new }
    let(:query_params) { {} }

    before do
      rack_headers = headers.transform_keys do |key|
        "HTTP_#{key.upcase.tr('-', '_')}"
      end
      app.call(rack_headers)
    end

    context 'when headers injected' do
      let(:parent_span) { tracer.start_span('wrap_span') }
      let(:headers) do
        super().tap do |headers|
          tracer.inject(parent_span.context, OpenTracing::FORMAT_RACK, headers)
        end
      end

      it 'crete span with parent span' do
        expect(tracer).to have_span('inner_span').with_parent
      end
    end

    context 'when headers not injected' do
      it 'crete span without parent span' do
        expect(tracer).to have_span('inner_span').child_of(nil)
      end
    end
  end
end
