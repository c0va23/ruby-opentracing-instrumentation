# frozen_string_literal: true

require 'spec_helper'
require 'opentracing/instrumentation/mongo'

RSpec.describe OpenTracing::Instrumentation::Mongo::TraceSubscriber do
  let(:tracer) { Test::Tracer.new }
  let(:sanitazer) { OpenTracing::Instrumentation::Mongo::DirectSanitazer.new }
  let(:subscriber) { described_class.new(tracer: tracer, sanitazer: sanitazer) }

  describe '#started' do
    let(:address) { 'localhost:27017' }
    let(:collection_name) { 'users' }
    let(:command_args) { { fileter: {} } }
    let(:command_name) { 'find' }
    let(:database_name) { 'main_db' }
    let(:operation_id) { rand(100) }
    let(:request_id) { rand(100) }
    let(:command_started) do
      instance_double(
        'CommandStarted',
        address: address,
        command: command_args.merge(command_name => collection_name),
        command_name: command_name,
        database_name: database_name,
        operation_id: operation_id,
        request_id: request_id,
      )
    end
    let(:operation_name) do
      "mongo(collection=#{database_name}.#{collection_name}, " \
        "command=#{command_name})"
    end

    before { subscriber.started(command_started) }

    it 'create not finished spin' do
      expect(tracer).to have_span(operation_name).in_progress
    end

    it 'create span with db tags' do
      expect(tracer).to have_span(operation_name).with_tags(
        'component' => 'db',
        'db.type' => 'mongo',
        'db.instance' => address,
      )
    end

    it 'create span with mongo tags' do
      expect(tracer).to have_span(operation_name).with_tags(
        'mongo.database_name' => database_name,
        'mongo.collection_name' => collection_name,
      )
    end

    it 'create span with request id tags' do
      expect(tracer).to have_span(operation_name).with_tags(
        'mongo.request_id' => request_id,
        'mongo.operation_id' => operation_id,
      )
    end

    it 'create span with command tags' do
      expect(tracer).to have_span(operation_name).with_tags(
        'mongo.command_name' => command_name,
        'mongo.command_args' => JSON.dump(command_args),
      )
    end

    describe '#succeeded' do
      let(:command_succeeded) do
        instance_double(
          'CommandSucceded',
          operation_id: operation_id,
        )
      end

      before { subscriber.succeeded(command_succeeded) }

      it 'close span' do
        expect(tracer).to have_span(operation_name).finished
      end

      it 'not tag error' do
        expect(tracer).not_to have_span(operation_name).with_tags(
          'error' => true,
        )
      end
    end

    describe '#failed' do
      let(:error_message) { 'DB error' }
      let(:failure) { { key: 'value' } }
      let(:command_failed) do
        instance_double(
          'CommandFailed',
          operation_id: operation_id,
          failure: failure,
          message: error_message,
        )
      end

      before { subscriber.failed(command_failed) }

      it 'close span with error' do
        expect(tracer).to have_span(operation_name).finished.with_tags(
          'error' => true,
        )
      end
    end
  end
end
