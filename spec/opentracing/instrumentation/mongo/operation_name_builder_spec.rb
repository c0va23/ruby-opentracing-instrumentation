# frozen_string_literal: true

require 'spec_helper'

RSpec.describe OpenTracing::Instrumentation::Mongo::OperationNameBuilder do
  let(:builder) { described_class.new }

  describe '#build_operation_name' do
    subject(:operation_name) { builder.build_operation_name(event) }

    let(:collection) { 'users' }
    let(:database) { 'main_db' }

    context 'when event with standart command' do
      let(:event) do
        instance_double(
          'Mongo::Event',
          command_name: 'find',
          command: {
            'find' => collection,
          },
          database_name: database,
        )
      end

      it 'return opeartion name with collection name' do
        expected_operation_name = \
          "mongo(collection=#{database}.#{collection}, command=find)"
        expect(operation_name).to eq expected_operation_name
      end
    end

    context 'when event with getMore command' do
      let(:cursor_id) { rand(1_000_000) }
      let(:event) do
        instance_double(
          'Mongo::Event',
          command_name: 'getMore',
          command: {
            'find' => cursor_id,
            'collection' => collection,
          },
          database_name: database,
        )
      end

      it 'return opeartion name without cursor_id' do
        expect(operation_name).not_to include cursor_id.to_s
      end

      it 'return opeartion name with collection name' do
        expected_operation_name = \
          "mongo(collection=#{database}.#{collection}, command=getMore)"
        expect(operation_name).to eq expected_operation_name
      end
    end

    context 'when event with aggreate command with collection' do
      let(:event) do
        instance_double(
          'Mongo::Event',
          command_name: 'aggregate',
          command: {
            'aggregate' => collection,
          },
          database_name: database,
        )
      end

      it 'return opeartion name with collection name' do
        expected_operation_name = \
          "mongo(collection=#{database}.#{collection}, command=aggregate)"
        expect(operation_name).to eq expected_operation_name
      end
    end

    context 'when event with aggreate command without collection' do
      let(:event) do
        instance_double(
          'Mongo::Event',
          command_name: 'aggregate',
          command: {
            'aggregate' => 1,
          },
          database_name: database,
        )
      end

      it 'return opeartion name without collection name' do
        expected_operation_name = \
          "mongo(collection=#{database}.1, command=aggregate)"
        expect(operation_name).to eq expected_operation_name
      end
    end
  end
end
