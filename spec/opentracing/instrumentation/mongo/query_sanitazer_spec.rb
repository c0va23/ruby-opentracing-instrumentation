# frozen_string_literal: true

require 'spec_helper'
require 'opentracing/instrumentation/mongo'

RSpec.describe OpenTracing::Instrumentation::Mongo::QuerySanitazer do
  let(:query_sanitazer) { described_class.new }

  describe '#sanitaze' do
    subject(:sanitazed_command) do
      query_sanitazer.sanitaze(command, command_name)
    end

    let(:command_name) { 'find' }
    let(:collection_name) { 'users' }
    let(:find_filter) { {} }
    let(:command_args) { { 'filter' => find_filter } }
    let(:command) { { command_name => collection_name }.merge(command_args) }

    context 'when command empty filter' do
      it 'return empty command' do
        expect(sanitazed_command).to eq('filter' => {})
      end
    end

    context 'when command have filter with string value' do
      let(:find_filter) { { 'name' => { '$eq' => 'username' } } }

      it 'replace string value with placeholder' do
        expect(sanitazed_command).to eq(
          'filter' => { 'name' => { '$eq' => '$string' } },
        )
      end
    end

    context 'when command have filter with object_id in array' do
      let(:find_filter) do
        {
          '_id' => {
            '$in' => [
              BSON::ObjectId.new,
              BSON::ObjectId.new,
            ],
          },
        }
      end

      it 'replace object_id values with placeholder' do
        expect(sanitazed_command).to eq(
          'filter' => { '_id' => { '$in' => ['$oid', '$oid'] } },
        )
      end
    end

    context 'when command have filter with large array' do
      let(:find_filter) do
        {
          '_id' => {
            '$in' => Array.new(5) { BSON::ObjectId.new },
          },
        }
      end

      it 'replace object_id values with placeholder' do
        expect(sanitazed_command).to eq(
          'filter' => { '_id' => { '$in' => ['$oid', '$oid', 'SKIPPED 1 ITEMS', '$oid', '$oid'] } },
        )
      end
    end

    context 'when command have filter with too large array' do
      let(:find_filter) do
        {
          '_id' => {
            '$in' => Array.new(10) { BSON::ObjectId.new },
          },
        }
      end

      it 'replace object_id values with placeholder' do
        expect(sanitazed_command).to eq(
          'filter' => { '_id' => { '$in' => ['$oid', '$oid', 'SKIPPED 6 ITEMS', '$oid', '$oid'] } },
        )
      end
    end

    context 'when command have filter with date' do
      let(:filter_date) { Time.current }
      let(:find_filter) { { 'created_at' => { '$gt' => filter_date } } }

      it 'not replace date value with placeholder' do
        expect(sanitazed_command).to eq(
          'filter' => { 'created_at' => { '$gt' => filter_date } },
        )
      end
    end

    context 'when command have filter with boolean' do
      let(:find_filter) { { 'active' => { '$eq' => true } } }

      it 'not replace boolean value with placeholder' do
        expect(sanitazed_command).to eq(
          'filter' => { 'active' => { '$eq' => true } },
        )
      end
    end

    context 'when command have filter with number' do
      let(:find_filter) { { 'balance' => { '$gt' => 100 } } }

      it 'not replace number value with placeholder' do
        expect(sanitazed_command).to eq(
          'filter' => { 'balance' => { '$gt' => 100 } },
        )
      end
    end

    context 'when command have internal attributes' do
      let(:read_preference_value) do
        {
          'mode' => 'primary',
        }
      end
      let(:command_args) do
        super().merge('$readPreference' => read_preference_value)
      end

      it 'not filter internal arguments' do
        expect(sanitazed_command).to include(
          '$readPreference' => read_preference_value,
        )
      end
    end
  end
end
