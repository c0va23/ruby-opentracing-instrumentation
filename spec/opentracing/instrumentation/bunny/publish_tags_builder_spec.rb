# frozen_string_literal: true

require 'spec_helper'

RSpec.describe OpenTracing::Instrumentation::Bunny::PublishTagsBuilder do
  let(:tracer) { Test::Tracer.new }
  let(:builder) { described_class.new }

  describe '#build_tags' do
    subject(:tags) { builder.build_tags(exchange, opts) }

    let(:exchange) do
      instance_double(
        'Exchange',
        name: 'test-exchange',
        type: 'topic',
      )
    end
    let(:opts) { {} }

    it 'return exchange tags' do
      expect(tags).to include(
        'amqp.exchange' => exchange.name,
        'amqp.exchange_type' => exchange.type,
      )
    end

    it 'return default content_type tag' do
      expect(tags).to include(
        'amqp.content_type' => described_class::DEFAULT_CONTENT_TYPE,
      )
    end

    it 'return default priority and persistent tags' do
      expect(tags).to include(
        'amqp.priority' => described_class::DEFAAUL_PRIORITY,
        'amqp.persistent' => described_class::DEFAULT_PERSISTENT,
      )
    end

    context 'with routing_key' do
      let(:opts) { super().merge(routing_key: 'test-key') }

      it 'return routing_key tags' do
        expect(tags).to include(
          'amqp.routing_key' => opts[:routing_key],
        )
      end
    end

    context 'with content type' do
      let(:opts) { super().merge(content_type: 'application/json') }

      it 'return content_type tags' do
        expect(tags).to include(
          'amqp.content_type' => opts[:content_type],
        )
      end
    end

    context 'with message_id' do
      let(:opts) { super().merge(message_id: SecureRandom.uuid) }

      it 'return message_id tag' do
        expect(tags).to include(
          'amqp.message_id' => opts[:message_id],
        )
      end
    end

    context 'with expiration' do
      let(:opts) { super().merge(expiration: 60_000) }

      it 'return expiration tags' do
        expect(tags).to include(
          'amqp.expiration' => opts[:expiration],
        )
      end
    end

    context 'with priority and persistent' do
      let(:opts) do
        super().merge(
          priority: 5,
          persistent: false,
        )
      end

      it 'return custom priority and persistent tags' do
        expect(tags).to include(
          'amqp.priority' => 5,
          'amqp.persistent' => false,
        )
      end
    end
  end
end
