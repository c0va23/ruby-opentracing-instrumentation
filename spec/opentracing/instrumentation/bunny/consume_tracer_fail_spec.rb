# frozen_string_literal: true

require 'spec_helper'
require_relative './consume_tracer_shared_contexts'

RSpec.describe OpenTracing::Instrumentation::Bunny::ConsumeTracer do
  let(:tracer) { Test::Tracer.new }

  describe '#consume' do
    include_context 'with bunny consume tracer'

    let(:span_exception) { StandardError.new('span error') }

    before do
      allow(operation_name_builder).to receive(:build_operation_name)
        .with(delivery_info).and_raise(span_exception)
    end

    context 'with block, but span create failed' do
      subject(:consume) do
        consume_tracer.consume(delivery_info, properties) do
          block_result
        end
      end

      let(:block_result) { instance_double('result') }

      it 'not create span' do
        consume
        expect(tracer).not_to have_span(operation_name)
      end

      it 'return result' do
        expect(consume).to eq(block_result)
      end

      it 'log error' do
        consume
        expect(logger).to have_received(:error)
      end
    end

    context 'without block, but span create failed' do
      subject(:consume) do
        consume_tracer.consume(delivery_info, properties)
      end

      it 'not return span' do
        consume
        expect(tracer).not_to have_span(operation_name)
      end

      it 'return nil' do
        expect(consume).to be nil
      end

      it 'log error' do
        consume
        expect(logger).to have_received(:error)
      end
    end
  end
end
