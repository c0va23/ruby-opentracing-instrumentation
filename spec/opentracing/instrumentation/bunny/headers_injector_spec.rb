# frozen_string_literal: true

require 'spec_helper'

RSpec.describe OpenTracing::Instrumentation::Bunny::HeadersInjector do
  let(:tracer) { Test::Tracer.new }
  let(:injector) { described_class.new(tracer: tracer) }

  describe '#inject' do
    let(:headers) { {} }

    around do |spec|
      tracer.start_active_span('wrapper') do
        spec.call
      end
    end

    it 'inject trace headers' do
      expect { injector.inject(headers) }.to(change { headers })
    end
  end
end
