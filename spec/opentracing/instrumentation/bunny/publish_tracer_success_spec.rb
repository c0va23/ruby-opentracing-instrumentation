# frozen_string_literal: true

require 'spec_helper'
require_relative './publish_tracer_shared_contexts'

RSpec.describe OpenTracing::Instrumentation::Bunny::PublishTracer do
  include_context 'with bunny publish tracer'

  describe '#publish' do
    subject(:publish) { publish_tracer.publish(payload, **opts) }

    let(:opts) { { routing_key: 'key' } }
    let(:headers) { { 'trace_id' => trace_id } }
    let(:payload) { instance_double('Payload') }
    let(:trace_id) { SecureRandom.uuid }
    let(:operation_name) { 'bunny_publish' }
    let(:tags) { { 'amqp' => 'ok' } }

    before do
      allow(operation_name_builder).to receive(:build_operation_name)
        .with(exchange, opts).and_return(operation_name)
      allow(tags_builder).to receive(:build_tags)
        .with(exchange, opts).and_return(tags)
      allow(injector).to receive(:inject) do |headers|
        headers['trace_id'] = trace_id
      end
    end

    context 'when publish with success' do
      before do
        allow(exchange).to receive(:publish)
          .with(payload, headers: headers, **opts)
      end

      it 'call exchange publish' do
        publish
        expect(exchange).to have_received(:publish)
      end

      it 'create span' do
        publish
        expect(tracer).to have_span(operation_name)
          .finished.with_tags(tags)
      end

      it 'create span without error' do
        publish
        expect(tracer).not_to have_span(operation_name)
          .with_tags('error' => true)
      end
    end

    context 'when publish raise error' do
      let(:publish_exception) { StandardError.new('publish exception') }

      before do
        allow(exchange).to receive(:publish)
          .with(payload, headers: headers, **opts)
          .and_raise(publish_exception)
      end

      it 'reraise error' do
        expect { publish }.to raise_error(publish_exception)
      end

      it 'create span with error' do
        publish rescue nil
        expect(tracer).to have_span(operation_name)
          .finished.with_tags('error' => true)
      end
    end
  end
end
