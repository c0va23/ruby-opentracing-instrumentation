# frozen_string_literal: true

require 'spec_helper'

RSpec.describe OpenTracing::Instrumentation::Bunny::PublishOperationNameBuilder do
  let(:tracer) { Test::Tracer.new }
  let(:builder) do
    described_class.new(routing_key_sanitazer: routing_key_sanitazer)
  end

  describe '#build_operation_name' do
    subject(:operation_name) do
      builder.build_operation_name(exchange, opts)
    end

    let(:exchange) { instance_double('Exchange', name: 'test-exchange') }
    let(:routing_key_sanitazer) do
      instance_double('RoutingKeySanitazer')
    end
    let(:sanitazed_routing_key) { 'sanitazed-key' }

    context 'when routing_key not nil' do
      let(:opts) { { routing_key: 'test-key' } }

      before do
        allow(routing_key_sanitazer).to receive(:sanitaze_routing_key)
          .with(opts[:routing_key]).and_return(sanitazed_routing_key)
      end

      it 'return operation name' do
        expected_operation_name = \
          'bunny_publish(routing_key=sanitazed-key, exchange=test-exchange)'
        expect(operation_name).to eq expected_operation_name
      end
    end

    context 'when routing_key is nil' do
      let(:opts) { { routing_key: nil } }

      before do
        allow(routing_key_sanitazer).to receive(:sanitaze_routing_key)
      end

      it 'return operation name with nil routing key' do
        expected_operation_name = \
          'bunny_publish(routing_key=, exchange=test-exchange)'
        expect(operation_name).to eq expected_operation_name
      end

      it 'not call sanitaze routing key' do
        operation_name
        expect(routing_key_sanitazer).not_to have_received(:sanitaze_routing_key)
      end
    end
  end
end
