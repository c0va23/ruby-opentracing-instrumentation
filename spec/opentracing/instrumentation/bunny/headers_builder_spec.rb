# frozen_string_literal: true

require 'spec_helper'

RSpec.describe OpenTracing::Instrumentation::Bunny::HeadersBuilder do
  let(:tracer) { Test::Tracer.new }
  let(:injector) { instance_double('Injector') }
  let(:builder) { described_class.new(tracer: tracer, injector: injector) }
  let(:trace_id) { SecureRandom.uuid }

  before do
    allow(injector).to receive(:inject) do |headers|
      headers['trace_id'] = trace_id
    end
  end

  describe '#build' do
    subject(:headers) { builder.build }

    around do |spec|
      tracer.start_active_span('wrapper') do
        spec.call
      end
    end

    it 'build headers with trace keys' do
      expect(headers).to include 'trace_id' => trace_id
    end
  end
end
