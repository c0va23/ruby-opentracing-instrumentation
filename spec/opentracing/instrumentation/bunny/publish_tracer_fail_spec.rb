# frozen_string_literal: true

require 'spec_helper'
require_relative './publish_tracer_shared_contexts'

RSpec.describe OpenTracing::Instrumentation::Bunny::PublishTracer do
  include_context 'with bunny publish tracer'

  describe '#publish' do
    subject(:publish) { publish_tracer.publish(payload, **opts) }

    let(:opts) { { routing_key: 'key' } }
    let(:trace_id) { SecureRandom.uuid }
    let(:headers) { { 'trace_id' => trace_id } }
    let(:payload) { instance_double('Payload') }
    let(:operation_name) { 'bunny_publish' }
    let(:build_exception) { StandardError.new('build error') }

    before do
      allow(operation_name_builder).to receive(:build_operation_name)
        .with(exchange, opts).and_raise(build_exception)
      allow(injector).to receive(:inject) do |headers|
        headers['trace_id'] = trace_id
      end
    end

    context 'when publish with success, but start span raise error' do
      before do
        allow(exchange).to receive(:publish)
          .with(payload, headers: headers, **opts)
      end

      it 'call exchange publish' do
        publish
        expect(exchange).to have_received(:publish)
      end

      it 'not create span' do
        publish
        expect(tracer).not_to have_span(operation_name)
      end
    end

    context 'when publish raise error, but start span raise error' do
      let(:publish_exception) { StandardError.new('publish exception') }

      before do
        allow(exchange).to receive(:publish)
          .with(payload, headers: headers, **opts)
          .and_raise(publish_exception)
      end

      it 'reraise error' do
        expect { publish }.to raise_error(publish_exception)
      end

      it 'not create span' do
        publish rescue nil
        expect(tracer).not_to have_span(operation_name)
      end
    end
  end
end
