# frozen_string_literal: true

require 'spec_helper'

RSpec.shared_context 'with bunny publish tracer' do
  let(:exchange) { instance_double('Exchange') }
  let(:tracer) { Test::Tracer.new }
  let(:operation_name_builder) { instance_double('OperationNameBuilder') }
  let(:tags_builder) { instance_double('TagsBuilder') }
  let(:injector) { instance_double('Injector') }
  let(:publish_tracer) do
    described_class.new(exchange) do |config|
      config.tracer = tracer
      config.operation_name_builder = operation_name_builder
      config.tags_builder = tags_builder
      config.injector = injector
    end
  end
end
