# frozen_string_literal: true

require 'spec_helper'

RSpec.describe OpenTracing::Instrumentation::Bunny::ConsumeOperationNameBuilder do
  describe '#build_operation_name' do
    subject(:operation_name) do
      builder.build_operation_name(delivery_info)
    end

    let(:queue) { instance_double('Bunny::Queue', name: 'test-queue') }
    let(:consumer) { instance_double('Bunny::Consumer', queue: queue) }
    let(:delivery_info) do
      {
        routing_key: 'test-key',
        exchange: 'test-exchange',
        consumer: consumer,
      }
    end
    let(:sanitazed_routing_key) { 'sanitazed-key' }
    let(:routing_key_sanitazer) do
      instance_double('RoutingKeySanitazer')
    end

    context 'with default pattern' do
      let(:builder) do
        described_class.new(routing_key_sanitazer: routing_key_sanitazer)
      end

      before do
        allow(routing_key_sanitazer).to receive(:sanitaze_routing_key)
          .with(delivery_info[:routing_key]).and_return(sanitazed_routing_key)
      end

      it 'return valid operation name' do
        expected_command_name = \
          'bunny_consume(routing_key=sanitazed-key, ' \
            'exchange=test-exchange, queue=test-queue)'
        expect(operation_name).to eq(expected_command_name)
      end
    end

    context 'when routing_key is nil' do
      let(:builder) do
        described_class.new(routing_key_sanitazer: routing_key_sanitazer)
      end
      let(:delivery_info) { super().merge(routing_key: nil) }

      before do
        allow(routing_key_sanitazer).to receive(:sanitaze_routing_key)
      end

      it 'not call sanitaze' do
        operation_name
        expect(routing_key_sanitazer).not_to have_received(:sanitaze_routing_key)
      end
    end
  end
end
