# frozen_string_literal: true

require 'spec_helper'
require_relative './consume_tracer_shared_contexts'

RSpec.describe OpenTracing::Instrumentation::Bunny::ConsumeTracer do
  let(:tracer) { Test::Tracer.new }

  describe '#consume' do
    include_context 'with bunny consume tracer'

    before do
      allow(operation_name_builder).to receive(:build_operation_name)
        .with(delivery_info).and_return(operation_name)

      allow(tags_builder).to receive(:build_tags)
        .with(delivery_info, properties).and_return(tags)
    end

    shared_examples 'create span without error' do
      before { consume }

      it 'create span' do
        expect(tracer).to have_span(operation_name).with_tags(tags)
      end

      it 'not create span with error tags' do
        expect(tracer).not_to have_span(operation_name).with_tags(
          'error' => true,
        )
      end
    end

    context 'with block returned result' do
      subject(:consume) do
        consume_tracer.consume(delivery_info, properties) do
          block_result
        end
      end

      let(:block_result) { instance_double('block_result') }

      include_examples 'create span without error'

      it 'return block result' do
        expect(consume).to eq(block_result)
      end
    end

    context 'with block returned result, but headers is nil' do
      subject(:consume) do
        consume_tracer.consume(delivery_info, properties) do
          block_result
        end
      end

      let(:properties) { super().merge(headers: nil) }
      let(:block_result) { instance_double('block_result') }

      include_examples 'create span without error'

      it 'not log error' do
        consume
        expect(logger).not_to have_received(:error)
      end
    end

    context 'when block raised error' do
      subject(:consume) do
        consume_tracer.consume(delivery_info, properties) do
          raise block_exception
        end
      end

      let(:block_exception) { StandardError.new('block error') }

      it 'raise exception' do
        expect { consume }.to raise_error(block_exception)
      end

      it 'create finished span with error' do
        consume rescue nil

        expect(tracer).to have_span(operation_name).finished.with_tags(
          'error' => true,
        )
      end
    end

    context 'without block' do
      subject(:consume) do
        consume_tracer.consume(delivery_info, properties)
      end

      include_examples 'create span without error'

      it 'return scope' do
        expect(consume).to be_a(Test::Scope)
      end

      it 'return span registered into tracer' do
        expect(consume.span).to eq(tracer.spans.first)
      end
    end
  end
end
