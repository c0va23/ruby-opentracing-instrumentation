# frozen_string_literal: true

require 'spec_helper'

RSpec.describe OpenTracing::Instrumentation::Bunny::ConsumeTagsBuilder do
  describe '#build_tags' do
    subject(:tags) do
      builder.build_tags(delivery_info, properties)
    end

    let(:queue) { instance_double('Bunny::Queue', name: 'queue-name') }
    let(:consumer) do
      instance_double(
        'Bunny::Consumer',
        queue: queue,
        no_ack: false,
        exclusive: '',
      )
    end
    let(:routing_key) { 'routing-key' }
    let(:exchange) { 'exchange-name' }
    let(:delivery_info) do
      {
        consumer: consumer,
        redelivered: false,
        exchange: exchange,
        routing_key: routing_key,
      }
    end
    let(:content_type) { 'application/json' }
    let(:properties) do
      {
        content_type: content_type,
      }
    end

    context 'with default settings' do
      let(:builder) { described_class.new }

      it 'return default static tags' do
        expect(tags).to include described_class::DEFAULT_STATIC_TAGS
      end

      it 'return properties tags' do
        expect(tags).to include(
          'amqp.content_type' => 'application/json',
        )
      end

      it 'return delivery_info tags' do
        expect(tags).to include(
          'amqp.redelivered' => false,
          'amqp.exchange' => exchange,
          'amqp.routing_key' => routing_key,
        )
      end

      it 'return consumer tags' do
        expect(tags).to include(
          'amqp.queue' => queue.name,
          'amqp.no_ack' => consumer.no_ack,
          'amqp.exclusive' => consumer.exclusive,
        )
      end
    end
  end
end
