# frozen_string_literal: true

require 'spec_helper'

RSpec.shared_context 'with bunny consume tracer' do
  let(:consume_tracer) do
    described_class.new do |config|
      config.tracer = tracer
      config.operation_name_builder = operation_name_builder
      config.tags_builder = tags_builder
      config.logger = logger
    end
  end
  let(:logger) { instance_double('Logger', error: nil) }
  let(:delivery_info) { instance_double('deliver info') }
  let(:headers) { {} }
  let(:properties) { { headers: headers } }
  let(:operation_name_builder) { instance_double('OperationNameBuilder') }
  let(:operation_name) { 'bunny_consume' }
  let(:tags_builder) { instance_double('TagBuilder') }
  let(:tags) { { 'span.kind' => 'consumer', 'amqp.test' => true } }
end
