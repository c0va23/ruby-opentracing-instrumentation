# frozen_string_literal: true

require 'spec_helper'

RSpec.describe OpenTracing::Instrumentation::Bunny::RegexpRoutingKeySanitazer do
  let(:sanitazer) { described_class.new }

  shared_examples 'sanitaze routing key' do |source, target|
    it 'return sanitaze routing key' do
      expect(sanitazer.sanitaze_routing_key(source)).to eq target
    end
  end

  describe '#routing_key' do
    include_examples 'sanitaze routing key',
                     'simple_key',
                     'simple_key'
    include_examples 'sanitaze routing key',
                     'simple.key',
                     'simple.key'
    include_examples 'sanitaze routing key',
                     'prefix.12345',
                     'prefix.:sequence_id'
    include_examples 'sanitaze routing key',
                     '12345.suffix',
                     ':sequence_id.suffix'
    include_examples 'sanitaze routing key',
                     'prefix.12345.suffix',
                     'prefix.:sequence_id.suffix'
    include_examples 'sanitaze routing key',
                     'prefix.1234567890abcdef12345678.suffix',
                     'prefix.:object_id.suffix'
    include_examples 'sanitaze routing key',
                     'prefix.1234567890abcdef12345678',
                     'prefix.:object_id'
  end
end
