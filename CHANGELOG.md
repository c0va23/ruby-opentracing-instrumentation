## 0.2.1
- Truncate arrays into mongo command sanitazer

## 0.2.0
- Drop support ruby 2.4
- Support ruby 3.0
- Update rubocop and fix new warnings
