# frozen_string_literal: true

module OpenTracing
  module Instrumentation
    module Thrift
      # Thrift processor trace operation name builder
      class TracedProcessorOperationNameBuilder
        DEFAULT_OPERATION_NAME_TEMPALTE = \
          'thrift_processor(%<name>s)'

        # @param operation_name_tempalte [String]
        def initialize(
          operation_name_template: DEFAULT_OPERATION_NAME_TEMPALTE
        )
          @operation_name_template = operation_name_template
        end

        # @param name [String] method name
        # @param type [Integer] type of message
        # @param seq_id [Integer] numberr of message
        # @return [String] operation name
        def build(name, type, seq_id)
          format_args = build_format_args(name, type, seq_id)
          format(operation_name_template, **format_args)
        end

        private

        attr_reader :operation_name_template

        def build_format_args(name, type, seq_id)
          {
            name: name,
            type: MESSAGE_TYPES[type],
            seq_id: seq_id,
          }
        end
      end
    end
  end
end
