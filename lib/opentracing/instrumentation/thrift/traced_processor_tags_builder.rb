# frozen_string_literal: true

module OpenTracing
  module Instrumentation
    module Thrift
      # Tags builder for TracedProcessor
      class TracedProcessorTagsBuilder < TracedProtocolTagsBuilder
        DEFAULT_STATIC_TAGS = {
          'span.kind' => 'server',
          'component' => 'thrift',
        }.freeze

        def initialize(static_tags: DEFAULT_STATIC_TAGS)
          super()
          @static_tags = static_tags
        end

        def build_tags(protocol, name, type)
          static_tags
            .merge(build_protocol_tags(protocol))
            .merge(build_message_tags(name, type))
        end

        private

        attr_reader :static_tags
      end
    end
  end
end
