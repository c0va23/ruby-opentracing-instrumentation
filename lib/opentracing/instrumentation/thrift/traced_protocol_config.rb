# frozen_string_literal: true

module OpenTracing
  module Instrumentation
    module Thrift
      # Config for TracedProtocol
      class TracedProtocolConfig
        attr_accessor :tracer,
                      :tags_builder,
                      :operation_name_builder,
                      :error_writer,
                      :logger

        def initialize(
          tracer: OpenTracing.global_tracer,
          tags_builder: TracedProtocolTagsBuilder.new,
          operation_name_builder: TracedProtocolOperationNameBuilder.new,
          error_writer: Common::ErrorWriter.new,
          logger: nil
        )
          @tracer = tracer
          @tags_builder = tags_builder
          @operation_name_builder = operation_name_builder
          @error_writer = error_writer
          @logger = logger
        end

        def ==(other)
          tracer == other.tracer &&
            tags_builder == other.tags_builder &&
            operation_name_builder == other.operation_name_builder &&
            error_writer == other.error_writer
        end
      end
    end
  end
end
