# frozen_string_literal: true

require 'thrift'

module OpenTracing
  module Instrumentation
    module Thrift
      # TagsBuilder for TracedProtocol
      class TracedProtocolTagsBuilder
        def build_message_tags(name, type)
          service_name, method = parse_message_name(name)
          {
            'thrift.method' => method,
            'thrift.service_name' => service_name,
            'thrift.multiplexed' => !service_name.nil?,
            'thrift.type' => MESSAGE_TYPES[type],
          }.merge(error_tags(type))
        end

        def build_protocol_tags(protocol)
          transport = protocol.trans
          {
            'thrift.protocol' => build_protocol_name(protocol),
            'thrift.transport' => build_transport_name(transport),
          }
        end

        def ==(other)
          self.class == other.class
        end

        private

        NAME_PATTER = /(?:(?<service_name>\w+):)?(?<method>\w+)/.freeze
        SERVICE_NAME_PART = 'service_name'
        METHOD_PART = 'method'

        def parse_message_name(name)
          name_matches = NAME_PATTER.match(name)
          method = name_matches[METHOD_PART]
          service_name = name_matches[SERVICE_NAME_PART]
          [service_name, method]
        end

        def build_protocol_name(protocol)
          protocol.class.to_s
        end

        def build_transport_name(transport)
          inner_transport = transport.instance_variable_get(:@transport)

          if inner_transport
            inner_transport_name = build_transport_name(inner_transport)
            "#{transport.class}(#{inner_transport_name})"
          else
            transport.class.to_s
          end
        end

        def error_tags(type)
          return {} if type != ::Thrift::MessageTypes::EXCEPTION

          {
            'error' => true,
          }
        end
      end
    end
  end
end
