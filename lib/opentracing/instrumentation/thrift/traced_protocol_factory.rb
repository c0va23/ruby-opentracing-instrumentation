# frozen_string_literal: true

require 'opentracing/instrumentation/thrift/traced_protocol'

module OpenTracing
  module Instrumentation
    module Thrift
      # Factory of TracedProtocol. Can be used thrift processor
      #
      # Usage (config.ru):
      #   multiplexed_processor = Thrift::MultiplexedProcessor.new
      #   binary_protocol_factory =
      #     OpenTracing::Instrumentation::Thrift::TracedProtocolFactory.new(
      #       Thrift::BinaryProtocolAcceleratedFactory.new,
      #     )
      #   json_protocol_factory =
      #     OpenTracing::Instrumentation::Thrift::TracedProtocolFactory.new(
      #       Thrift::JsonProtocolFactory.new,
      #     )
      #   protocol_factoreis_map = {
      #     binary_protocol_factory => ['application/x-thrift'],
      #     json_protocol_factory => ['application/json'],
      #   }
      #   thrift_app =
      #     ::MultiprotocolThriftRackApp.new(
      #       multiplexed_processor,
      #       protocol_factoreis_map,
      #     )
      #   run thrift_app
      class TracedProtocolFactory < ::Thrift::BaseProtocolFactory
        def initialize(protocol_factory, config: TracedProtocolConfig.new)
          super()
          @protocol_factory = protocol_factory
          @config = config
        end

        def get_protocol(trans)
          protocol = protocol_factory.get_protocol(trans)
          TracedProtocol.new(protocol, config: config)
        end

        private

        attr_reader :protocol_factory,
                    :config
      end
    end
  end
end
