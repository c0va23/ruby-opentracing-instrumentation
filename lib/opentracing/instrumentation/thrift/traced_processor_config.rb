# frozen_string_literal: true

module OpenTracing
  module Instrumentation
    module Thrift
      # Config of ThriftProcessor
      class TracedProcessorConfig
        # @return [OpenTracing::Tracer]
        attr_accessor :tracer

        # @return [Boolean] enabled wrap protocol into TracedProtocol
        attr_accessor :trace_protocol

        # @return [TracedProcessorOperationNameBuilder]
        attr_accessor :operation_name_builder

        # @return [TracedProcessorTagsBuilder]
        attr_accessor :tags_builder

        # @return [Common::ErrorWriter]
        attr_accessor :error_writer

        # @return [Logger] used for log errors. If nil (by default), then logging disabled,
        attr_accessor :logger

        def initialize
          @tracer = OpenTracing.global_tracer
          @trace_protocol = true
          @operation_name_builder = TracedProcessorOperationNameBuilder.new
          @tags_builder = TracedProcessorTagsBuilder.new
          @error_writer = Common::ErrorWriter.new
          @logger = nil
        end
      end
    end
  end
end
