# frozen_string_literal: true

require 'thrift'

module OpenTracing
  module Instrumentation
    module Thrift
      # Build operation name for TracedProtocol
      class TracedProtocolOperationNameBuilder
        DEFAULT_OPERATION_NAME_PATTERN = \
          'thrift(direction=%<direction>s, name=%<name>s, type=%<type>s)'

        # @param operation_name_pattern [String]
        def initialize(
          operation_name_pattern: DEFAULT_OPERATION_NAME_PATTERN
        )
          @operation_name_pattern = operation_name_pattern
        end

        # @param direction [String] should be 'write' or 'read'
        # @param name [String] method name. Example: 'Service:method'
        # @param type [Integer] message type, See ::Thrift::MessageTypes
        # @return [String] formated operation name
        def build_operation_name(direction, name, type)
          format_args = build_format_args(direction, name, type)
          format(operation_name_pattern, **format_args)
        end

        def ==(other)
          operation_name_pattern == other.operation_name_pattern
        end

        protected

        attr_reader :operation_name_pattern

        private

        def build_format_args(direction, name, type)
          {
            direction: direction,
            name: name,
            type: MESSAGE_TYPES[type],
          }
        end
      end
    end
  end
end
