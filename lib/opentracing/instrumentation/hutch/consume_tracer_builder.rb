# frozen_string_literal: true

module OpenTracing
  module Instrumentation
    module Hutch
      # ConsumeTracerBuilder build and configure ConsumeTracer.
      # Should be used for configuration of ConsumeTracer.
      #
      # Usage:
      #   hutch_tracer_builder = \
      #     OpenTracing::Instrumentation::Hutch::ConsumeTracerBuilder.new do |config|
      #       config.tracer = CustomTracer.new
      #     end
      #   Hutch::Config.set(:tracer, hutch_tracer_builder)
      #
      # @param config [ConsumeTracerConfig]
      # @yield [ConsumeTracerConfig]
      class ConsumeTracerBuilder
        def initialize(config: ConsumeTracerConfig.new)
          yield config if block_given?
          @config = config.dup
        end

        # Build conifgured ConsumeTracer
        #
        # ! Its not constructor
        #
        # @return ConsumeTracer
        def new(consumer)
          ConsumeTracer.new(
            consumer,
            config: @config,
          )
        end
      end
    end
  end
end
