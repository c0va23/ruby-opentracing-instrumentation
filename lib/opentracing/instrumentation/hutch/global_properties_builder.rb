# frozen_string_literal: true

module OpenTracing
  module Instrumentation
    module Hutch
      # GlobalPropertiesBuilder build hutch global properties with
      # tracing headers
      #
      # Can extend other global properties builders.
      # Usage:
      #   Hutch.global_properties = \
      #     OpenTracing::Instrumentation::Hutch::GlobalPropertiesBuilder.new
      #
      class GlobalPropertiesBuilder
        # EmptyPropertiesBuilder is deafult properties build. It return
        # empty properties.
        class EmptyPropertiesBuilder
          def call
            {}
          end
        end

        # @param headers_injector [Bunny::HeadersInjector]
        # @param global_properties_builder [EmptyPropertiesBuilder]
        def initialize(
          headers_injector: Bunny::HeadersInjector.new,
          global_properties_builder: EmptyPropertiesBuilder.new
        )
          @headers_injector = headers_injector
          @global_properties_builder = global_properties_builder
        end

        # @return [Hash<String, String>] properties with injected tracing
        # headers
        def call
          properties = @global_properties_builder.call
          headers = (properties[:headers] ||= {})
          @headers_injector.inject(headers)
          properties
        end
      end
    end
  end
end
