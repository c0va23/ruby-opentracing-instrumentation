# frozen_string_literal: true

module OpenTracing
  module Instrumentation
    module Hutch
      # ConsumeOperationNameBuilder build span operation name of
      # Hutch ConsumeTracer
      class ConsumeOperationNameBuilder
        DEFAULT_OPERATION_NAME_PATTERN = \
          'hutch_consume(consumer_class=%<consumer_class>s)'

        # @param routing_key_sanitazer [Bunny::RegexpRoutingKeySanitazer]
        # @param operation_name_pattern [String]
        def initialize(
          routing_key_sanitazer: Bunny::RegexpRoutingKeySanitazer.new,
          operation_name_pattern: DEFAULT_OPERATION_NAME_PATTERN
        )
          @routing_key_sanitazer = routing_key_sanitazer
          @operation_name_pattern = operation_name_pattern
        end

        # @param consume [Object] instance of consumer
        # @param message [Hutch::Message]
        # @return [String] operation_name
        def build_operation_name(consumer, message)
          format_args = build_format_args(consumer, message)
          format(@operation_name_pattern, format_args)
        end

        private

        def build_format_args(consumer, message)
          consumer_format_args(consumer)
            .merge(delivery_info_format_args(message.delivery_info))
            .merge(message.properties.to_h)
        end

        def delivery_info_format_args(delivery_info)
          routing_key = delivery_info[:routing_key]
          sanitazed_routing_key = sanitaze_routing_key(routing_key)
          delivery_info
            .to_h
            .merge(routing_key: sanitazed_routing_key)
        end

        def sanitaze_routing_key(routing_key)
          return if routing_key.nil?

          @routing_key_sanitazer.sanitaze_routing_key(routing_key)
        end

        def consumer_format_args(consumer)
          {
            consumer_class: consumer.class.to_s,
          }
        end
      end
    end
  end
end
