# frozen_string_literal: true

module OpenTracing
  module Instrumentation
    module Hutch
      # ConsumeTagsBuilder build span tags for Hutch ConsumerTrace.
      # Its use Bunny::ComsumerTagsBuilde for common tags and add
      # hutch specific tags
      class ConsumeTagsBuilder
        DEFAULT_STATIC_TAGS = {
          'component' => 'hutch',
        }.freeze

        # @param bunny_consume_tags_builder [Bunny::ConsumeTagsBuilder]
        # @param static_tags [Hash<String, String>]
        def initialize(
          bunny_consume_tags_builder: Bunny::ConsumeTagsBuilder.new,
          static_tags: DEFAULT_STATIC_TAGS
        )
          @bunny_consume_tags_builder = bunny_consume_tags_builder
          @static_tags = static_tags
        end

        # @param consumer [Object] instance of consumer
        # @param message [Hutch::Message]
        # @return [Hash<String, String>] tags
        def build_tags(consumer, message)
          build_bunny_tags(message)
            .merge(hutch_tags(consumer))
            .merge(@static_tags)
        end

        private

        attr_reader :static_tags

        def build_bunny_tags(message)
          @bunny_consume_tags_builder.build_tags(
            message.delivery_info,
            message.properties,
          )
        end

        def hutch_tags(consumer)
          {
            'hutch.consumer_class' => consumer.class.to_s,
          }
        end
      end
    end
  end
end
