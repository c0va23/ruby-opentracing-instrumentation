# frozen_string_literal: true

module OpenTracing
  module Instrumentation
    module Hutch
      # ConsumeTracer trace Hutch consumer. Cannot be configured,
      # Use ConsumeTracerBuilder for configuration.
      #
      # Usage:
      #   Hutch::Config.set(
      #     :tracer,
      #     OpenTracing::Instrumentation::Hutch::ConsumeTracer,
      #   )
      class ConsumeTracer
        extend Forwardable

        def initialize(consumer, config: ConsumeTracerConfig.new)
          @consumer = consumer
          @config = config
        end

        # Method handle called by Hutch on message. This method
        # start active span, process consumer and close span.
        def handle(message)
          scope = safe_start_active_span(message)
          consumer.process(message)
        rescue StandardError => e
          error_writer.write_error(scope.span, e) if scope
          raise e
        ensure
          # Close scope if exists
          scope&.close
        end

        private

        attr_reader :consumer

        def_delegators(
          :@config,
          :tracer,
          :error_writer,
          :operation_name_builder,
          :tags_builder,
          :logger,
        )

        def safe_start_active_span(message)
          start_active_span(message)
        rescue StandardError => e
          logger.error(e)
          nil
        end

        def start_active_span(message)
          operation_name = build_operation_name(message)
          tags = tags_builder.build_tags(consumer, message)
          references = build_references(message.properties[:headers])

          tracer.start_active_span(
            operation_name,
            tags: tags,
            references: references,
          )
        end

        def build_operation_name(message)
          operation_name_builder.build_operation_name(
            consumer,
            message,
          )
        end

        def build_references(headers)
          return if headers.nil?

          span_context = tracer.extract(
            OpenTracing::FORMAT_TEXT_MAP,
            headers,
          )
          return if span_context.nil?

          [OpenTracing::Reference.follows_from(span_context)]
        end
      end
    end
  end
end
