# frozen_string_literal: true

module OpenTracing
  module Instrumentation
    module Hutch
      # Config for ConsumeTracer
      class ConsumeTracerConfig
        # @return [ConsumeOperationNameBuilder]
        attr_accessor :operation_name_builder

        # @return [ConsumeTagsBuilder]
        attr_accessor :tags_builder

        # @return [OpenTracing::Tracer]
        attr_accessor :tracer

        # @return [Common::ErrorWriter]
        attr_accessor :error_writer

        # @return [::Logger]
        attr_accessor :logger

        def initialize
          @operation_name_builder = ConsumeOperationNameBuilder.new
          @tags_builder = ConsumeTagsBuilder.new
          @tracer = OpenTracing.global_tracer
          @error_writer = Common::ErrorWriter.new
          @logger = ::Hutch::Logging.logger
        end
      end
    end
  end
end
