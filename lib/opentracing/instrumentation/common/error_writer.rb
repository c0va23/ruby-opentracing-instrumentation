# frozen_string_literal: true

require 'json'

module OpenTracing
  module Instrumentation
    module Common
      # ErrorWriter can be used to write error tag and log to span
      class ErrorWriter
        LOG_ERROR_EVENT = 'error'
        ERROR_TAG = 'error'

        attr_reader :set_error_tag,
                    :log_error_event

        # @param set_error_tag [TrueClass,ErrorClass] enable set error tag
        # @param log_error_event [TrueClass, ErrorClass] enable log exception
        def initialize(
          set_error_tag: true,
          log_error_event: true
        )
          @set_error_tag = set_error_tag
          @log_error_event = log_error_event
        end

        # Write error tag and log error event
        # @param span [OpenTracing::Span] target for tag and log
        # @param exception [Exception] logged to tag
        def write_error(span, exception, event: LOG_ERROR_EVENT)
          tag_error(span)

          log_error(span, exception, event)
        end

        def ==(other)
          set_error_tag == other.set_error_tag &&
            log_error_event == other.log_error_event
        end

        private

        def tag_error(span)
          return unless set_error_tag

          span.set_tag('error', true)
        end

        def log_error(span, exception, event)
          return unless log_error_event

          span.log_kv(
            event: event,
            'error.kind': exception.class.to_s,
            message: exception.to_s,
            stack: JSON.dump(exception.backtrace),
          )
        end
      end
    end
  end
end
