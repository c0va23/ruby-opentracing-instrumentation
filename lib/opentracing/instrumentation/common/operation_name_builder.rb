# frozen_string_literal: true

module OpenTracing
  module Instrumentation
    module Common
      # Build operation name by template and tags
      class OperationNameBuilder
        def initialize(operation_name_template:)
          @operation_name_template = operation_name_template
        end

        # build operation name with tags
        def build(tags)
          format(operation_name_template, **tags)
        end

        private

        attr_reader :operation_name_template
      end
    end
  end
end
