# frozen_string_literal: true

module OpenTracing
  module Instrumentation
    # Rack tracing middlewares
    module Rack
      autoload :ExtractMiddleware,
               'opentracing/instrumentation/rack/extract_middleware'
      autoload :HttpTagger, 'opentracing/instrumentation/rack/http_tagger'
      autoload :RegexpHostSanitazer,
               'opentracing/instrumentation/rack/regexp_host_sanitazer'
      autoload :RegexpPathSanitazer,
               'opentracing/instrumentation/rack/regexp_path_sanitazer'
      autoload :StaticCommandNameBuilder,
               'opentracing/instrumentation/rack/static_command_name_builder'
      autoload :TraceMiddleware, 'opentracing/instrumentation/rack/trace_middleware'
      autoload :UnsafePathSanitazer,
               'opentracing/instrumentation/rack/unsafe_path_sanitazer'
      autoload :UrlCommandNameBuilder,
               'opentracing/instrumentation/rack/url_command_name_builder'
    end
  end
end
