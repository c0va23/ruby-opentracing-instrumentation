# frozen_string_literal: true

module OpenTracing
  module Instrumentation
    # Faraday tracing middlewares
    module Faraday
      autoload :TraceMiddleware, 'opentracing/instrumentation/faraday/trace_middleware'
    end
  end
end
