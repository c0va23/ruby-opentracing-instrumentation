# frozen_string_literal: true

require 'thrift'

module OpenTracing
  module Instrumentation
    # OpenTracing instrumentation for Thrift client and server
    module Thrift
      module_path = 'opentracing/instrumentation/thrift'

      MESSAGE_TYPES = {
        ::Thrift::MessageTypes::CALL => 'CALL',
        ::Thrift::MessageTypes::REPLY => 'REPLY',
        ::Thrift::MessageTypes::EXCEPTION => 'EXCEPTION',
        ::Thrift::MessageTypes::ONEWAY => 'ONEWAY',
      }.freeze

      autoload :TracedProcessor,
               "#{module_path}/traced_processor"
      autoload :TracedProcessorConfig,
               "#{module_path}/traced_processor_config"
      autoload :TracedProcessorOperationNameBuilder,
               "#{module_path}/traced_processor_operation_name_builder"
      autoload :TracedProcessorTagsBuilder,
               "#{module_path}/traced_processor_tags_builder"
      autoload :TracedProtocol,
               "#{module_path}/traced_protocol"
      autoload :TracedProtocolConfig,
               "#{module_path}/traced_protocol_config"
      autoload :TracedProtocolFactory,
               "#{module_path}/traced_protocol_factory"
      autoload :TracedProtocolOperationNameBuilder,
               "#{module_path}/traced_protocol_operation_name_builder"
      autoload :TracedProtocolTagsBuilder,
               "#{module_path}/traced_protocol_tags_builder"
    end
  end
end
