# frozen_string_literal: true

require 'rack'

module OpenTracing
  module Instrumentation
    module Rack
      # UrlCommandNameBuilder build command name with request url
      class UrlCommandNameBuilder
        DEFAULT_COMMAND_PATTERN = \
          'rack(%<method>s %<schema>s://%<host>s%<path>s)'

        def initialize(
          command_pattern: DEFAULT_COMMAND_PATTERN,
          host_sanitazer: RegexpHostSanitazer.new,
          path_sanitazer: RegexpPathSanitazer.new
        )
          @command_pattern = command_pattern
          @host_sanitazer = host_sanitazer
          @path_sanitazer = path_sanitazer
        end

        def build_command_name(env)
          format(@command_pattern, pattern_args(env))
        end

        private

        def pattern_args(env)
          path = env[::Rack::REQUEST_PATH]
          sanitazed_path = @path_sanitazer.sanitaze_path(path)
          {
            schema: env[::Rack::RACK_URL_SCHEME],
            method: env[::Rack::REQUEST_METHOD],
            host: sanitaze_host(env[::Rack::HTTP_HOST]),
            port: env[::Rack::HTTP_PORT],
            path: sanitazed_path,
          }
        end

        def sanitaze_host(host)
          return if host.nil?

          @host_sanitazer.sanitaze_host(host)
        end
      end
    end
  end
end
