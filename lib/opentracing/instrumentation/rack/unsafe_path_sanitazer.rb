# frozen_string_literal: true

require 'rack'

module OpenTracing
  module Instrumentation
    module Rack
      # UnsafePathSanitazer return raw path
      class UnsafePathSanitazer
        def sanitaze_path(path)
          path
        end
      end
    end
  end
end
