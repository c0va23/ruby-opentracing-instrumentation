# frozen_string_literal: true

module OpenTracing
  module Instrumentation
    module Rack
      # StaticCommandNameBuilder build static command_name
      class StaticCommandNameBuilder
        DEFAULT_COMMAND_NAME = 'rack'

        def initialize(
          command_name: DEFAULT_COMMAND_NAME
        )
          @command_name = command_name
        end

        def build_command_name(_env)
          @command_name
        end
      end
    end
  end
end
