# frozen_string_literal: true

require 'rack'

module OpenTracing
  module Instrumentation
    module Rack
      # RegexpPathSanitazer return raw path
      class RegexpPathSanitazer
        DEFAULT_REPLACE_MAP = {
          '/:object_id\1' => %r{/[0-9a-f]{24}(/|$)},
          '/:sequence_id\1' => %r{/\d+(/|$)},
          '/:phone\1' => %r{/\+\d+(/|$)},
        }.freeze

        def initialize(
          replace_map: DEFAULT_REPLACE_MAP
        )
          @replace_map = replace_map
        end

        def sanitaze_path(path)
          @replace_map.each do |(target, regexp)|
            path = path.gsub(regexp, target)
          end
          path
        end
      end
    end
  end
end
