# frozen_string_literal: true

require 'rack'

module OpenTracing
  module Instrumentation
    module Rack
      # RegexpHostSanitazer sanitaze host
      class RegexpHostSanitazer
        DEFAULT_REPLACE_MAP = {
          '\1.0.0.0' => /^(\d+)\.\d+\.\d+\.\d+:\d+$/,
        }.freeze

        def initialize(replace_map: DEFAULT_REPLACE_MAP)
          @replace_map = replace_map
        end

        def sanitaze_host(host)
          @replace_map.each do |pattern, regexp|
            host = host.gsub(regexp, pattern)
          end
          host
        end
      end
    end
  end
end
