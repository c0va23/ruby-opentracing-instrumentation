# frozen_string_literal: true

require 'rack'

module OpenTracing
  module Instrumentation
    module Rack
      # HttpTagger create addition tags on span
      class HttpTagger
        DEFAULT_TAG_REQUEST_HEADERS = {
          connection: 'Connection',
          content_type: 'Content-Type',
          user_agent: 'User-Agent',
        }.freeze

        DEFAULT_TAG_RESPONSE_HEADERS = {
          connection: 'Connection',
          content_type: 'Content-Type',
          keep_alive: 'Keep-Alive',
        }.freeze

        def initialize(
          tag_request_headers: DEFAULT_TAG_REQUEST_HEADERS,
          tag_response_headers: DEFAULT_TAG_RESPONSE_HEADERS
        )
          @tag_request_headers =
            prepare_request_mapping(tag_request_headers)
          @tag_response_headers =
            prepare_response_mapping(tag_response_headers)
        end

        def request_tags(env)
          @tag_request_headers
            .transform_values { |header_name| env[header_name] }
            .compact
        end

        def response_tags(headers)
          header_mapper = lambda do |header_regexp|
            headers.find do |(header, _value)|
              header_regexp.match?(header)
            end&.dig(1)
          end
          @tag_response_headers
            .transform_values(&header_mapper)
            .compact
        end

        private

        def prepare_request_mapping(source_mapping)
          source_mapping.map do |key, header|
            rack_header = "HTTP_#{header.tr('-', '_').upcase}"
            tag_name = "http.request.#{key}"
            [tag_name, rack_header]
          end.to_h
        end

        def prepare_response_mapping(source_mapping)
          source_mapping.map do |key, header|
            tag_name = "http.response.#{key}"
            header_regexp = /^#{header}$/i
            [tag_name, header_regexp]
          end.to_h
        end
      end
    end
  end
end
