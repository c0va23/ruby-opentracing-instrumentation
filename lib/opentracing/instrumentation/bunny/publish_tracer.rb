# frozen_string_literal: true

module OpenTracing
  module Instrumentation
    module Bunny
      # PublishTracer trace publishing and inject trace headers into
      # AMQP message
      #
      # Usage:
      #   exchange = channel.topic(BUNNY_EXCHANGE_NAME)
      #   default_publisher \
      #     = OpenTracing::Instrumentation::Bunny::PublishTracer.new(exchange)
      #   configured_publisher = \
      #     OpenTracing::Instrumentation::Bunny::PublishTracer.new(exchange) do |c|
      #       c.tracer = custom_tracer
      #     end
      #   publisher.publish(
      #     '{"message": 123}',
      #     routing_key: 'key',
      #   )
      class PublishTracer
        extend Forwardable

        # @param exchange [Bunny::Exchange]
        def initialize(exchange, config: PublishTracerConfig.new)
          @exchange = exchange
          yield config if block_given?
          @config = config.dup
        end

        # Publish message via call `exchange.publish`, with injecting tracing
        # into headers and create span around publising call.
        #
        # For deatils see:
        # https://www.rubydoc.info/gems/bunny/Bunny/Exchange#publish-instance_method
        #
        # @param active [OpenTracing::Span] allow overide span
        # @return [Bunny::Exchange]
        def publish(
          payload,
          active_span: tracer.active_span,
          **opts
        )
          trace_publish(span: active_span, **opts) do |scope|
            publish_with_headers(
              payload,
              span: scope&.span || active_span,
              **opts,
            )
          end
        end

        private

        attr_reader :exchange

        def_delegators(
          :@config,
          :tracer,
          :operation_name_builder,
          :tags_builder,
          :injector,
          :error_writer,
          :logger,
        )

        def safe_inject_headers(span:, headers:)
          injector.inject(headers, active_span: span)
        end

        def publish_with_headers(
          payload,
          span:,
          headers: {},
          **opts
        )
          safe_inject_headers(span: span, headers: headers)
          exchange.publish(
            payload,
            headers: headers,
            **opts,
          )
        end

        def trace_publish(span:, **opts)
          scope = safe_start_active_span(span: span, **opts)
          yield scope
        rescue StandardError => e
          error_writer.write_error(scope.span, e) if scope
          logger&.error(e)
          raise e
        ensure
          scope&.close
        end

        def safe_start_active_span(span:, **opts)
          start_active_span(span: span, **opts)
        rescue StandardError
          nil
        end

        def start_active_span(span:, **opts)
          operation_name = build_operation_name(opts)
          tags = build_tags(opts)
          tracer.start_active_span(
            operation_name,
            tags: tags,
            child_of: span,
          )
        end

        def build_operation_name(opts)
          operation_name_builder.build_operation_name(exchange, opts)
        end

        def build_tags(opts)
          tags_builder.build_tags(exchange, opts)
        end
      end
    end
  end
end
