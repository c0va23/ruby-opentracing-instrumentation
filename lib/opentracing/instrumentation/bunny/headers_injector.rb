# frozen_string_literal: true

module OpenTracing
  module Instrumentation
    module Bunny
      # HeadersInjector inject tracing headers into bunny message headers
      class HeadersInjector
        # @param trace [OpenTracing::Tracer]
        def initialize(tracer: OpenTracing.global_tracer)
          @tracer = tracer
        end

        # inject tracing headers
        # @param headers [Hash<String,String>]
        # @param active_span [OpenTracing::Span]
        def inject(
          headers,
          active_span: @tracer.active_span
        )
          @tracer.inject(
            active_span.context,
            OpenTracing::FORMAT_TEXT_MAP,
            headers,
          )
        end
      end
    end
  end
end
