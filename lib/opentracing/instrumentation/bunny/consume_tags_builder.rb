# frozen_string_literal: true

module OpenTracing
  module Instrumentation
    module Bunny
      # ConsumeTagsBuidler build consume tags from delivery_info and
      # properties
      class ConsumeTagsBuilder
        DEFAULT_STATIC_TAGS = {
          'span.kind' => 'consumer',
          'component' => 'bunny',
        }.freeze

        # @param static_tags [Hash<String, String>]
        def initialize(static_tags: DEFAULT_STATIC_TAGS)
          @static_tags = static_tags
        end

        # @param delivery_info [Bunny::DeliveryInfo]
        # @param properties [Bunny::MessageProperties]
        # @return [Hash] consume span tags
        def build_tags(delivery_info, properties)
          @static_tags
            .merge(build_properties_tags(properties))
            .merge(build_delivery_info_tags(delivery_info))
            .merge(build_consumer_tags(delivery_info[:consumer]))
        end

        private

        def build_properties_tags(properties)
          {
            'amqp.content_type' => properties[:content_type],
          }
        end

        def build_delivery_info_tags(delivery_info)
          {
            'amqp.redelivered' => delivery_info[:redelivered],
            'amqp.exchange' => delivery_info[:exchange],
            'amqp.routing_key' => delivery_info[:routing_key],
          }
        end

        def build_consumer_tags(consumer)
          {
            'amqp.queue' => consumer.queue.name,
            'amqp.no_ack' => consumer.no_ack,
            'amqp.exclusive' => consumer.exclusive,
          }
        end
      end
    end
  end
end
