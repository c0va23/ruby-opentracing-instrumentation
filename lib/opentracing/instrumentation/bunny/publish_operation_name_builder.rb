# frozen_string_literal: true

module OpenTracing
  module Instrumentation
    module Bunny
      # PublishOperationNameBuilder build publish command name from
      # exchange and publish options
      class PublishOperationNameBuilder
        DEFAULT_OPERATION_NAME_PATTERN = \
          'bunny_publish(' \
            'routing_key=%<routing_key>s, ' \
            'exchange=%<exchange>s' \
          ')'

        # @param routing_key_sanitazer [RegexpRoutingKeySanitazer]
        # @param operation_name_pattern [String]
        def initialize(
          routing_key_sanitazer: RegexpRoutingKeySanitazer.new,
          operation_name_pattern: DEFAULT_OPERATION_NAME_PATTERN
        )
          @routing_key_sanitazer = routing_key_sanitazer
          @operation_name_pattern = operation_name_pattern
        end

        # @param exchange [Bunny::Exchange]
        # @param opts [Hash<Symbol, Object>]
        # @option opts [String] :routing_key
        # @return [String]
        def build_operation_name(exchange, opts)
          format_args = build_format_args(exchange, opts)
          format(@operation_name_pattern, **format_args)
        end

        private

        def build_format_args(exchange, opts)
          opts
            .merge(exchange: exchange.name)
            .merge(routing_key: sanitaze_routing_key(opts[:routing_key]))
        end

        def sanitaze_routing_key(routing_key)
          return if routing_key.nil?

          @routing_key_sanitazer.sanitaze_routing_key(routing_key)
        end
      end
    end
  end
end
