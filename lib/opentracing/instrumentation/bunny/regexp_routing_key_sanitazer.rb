# frozen_string_literal: true

module OpenTracing
  module Instrumentation
    module Bunny
      # Replace id into routing key with placeholder
      #
      # Example:
      #    sanitazer = RegexpRoutingKeySanitazer.new
      #    sanitazer.sanitaze_routing_key('prefix.1234567890abcdef12345678')
      #
      #    # 'prefix.:object_id'
      #
      #
      #    sanitazer.sanitaze_routing_key('prefix.123.suffix')
      #
      #    # 'prefix.:sequence_id.suffis'
      class RegexpRoutingKeySanitazer
        ROUTING_KEY_SEPARATOR = '.'
        DEFAULT_REPLCE_REGEXP_MAP = {
          ':sequence_id' => /^\d+$/,
          ':object_id' => /^[0-9a-f]{24}$/,
        }.freeze

        # @param replace_regexp_map [Hash<String, String>]
        def initialize(replace_regexp_map: DEFAULT_REPLCE_REGEXP_MAP)
          @replace_regexp_map = replace_regexp_map
        end

        # @param routing_key [String] souce routing key
        # @return [String] sanitazed routing key
        def sanitaze_routing_key(routing_key)
          routing_key
            .split(ROUTING_KEY_SEPARATOR)
            .map { |part| filter_part(part) }
            .join(ROUTING_KEY_SEPARATOR)
        end

        private

        attr_reader :replace_regexp_map

        def filter_part(routing_key_part)
          replace_regexp_map.each do |placeholder, regexp|
            return placeholder if regexp.match?(routing_key_part)
          end
          routing_key_part
        end
      end
    end
  end
end
