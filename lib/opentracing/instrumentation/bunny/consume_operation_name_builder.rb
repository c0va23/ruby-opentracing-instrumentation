# frozen_string_literal: true

module OpenTracing
  module Instrumentation
    module Bunny
      # ConsumeOperationNameBuilder build consume command name from
      # queue and delivery_info
      class ConsumeOperationNameBuilder
        DEFAULT_OPERATION_NAME_PATTERN = \
          'bunny_consume(' \
            'routing_key=%<routing_key>s, ' \
            'exchange=%<exchange>s, ' \
            'queue=%<queue>s' \
          ')'

        # @param routing_key_sanitazer [RegexpRoutingKeySanitazer]
        # @param operation_name_pattern [String]
        def initialize(
          routing_key_sanitazer: RegexpRoutingKeySanitazer.new,
          operation_name_pattern: DEFAULT_OPERATION_NAME_PATTERN
        )
          @routing_key_sanitazer = routing_key_sanitazer
          @operation_name_pattern = operation_name_pattern
        end

        # @param delivery_info [Bunny::DeliveryInfo]
        # @return [String] bunny consume operation name
        def build_operation_name(delivery_info)
          format_args = build_format_args(delivery_info)
          format(@operation_name_pattern, format_args)
        end

        private

        def build_format_args(delivery_info)
          queue = delivery_info[:consumer].queue
          routing_key = \
            sanitaze_routing_key(delivery_info[:routing_key])
          delivery_info.to_h.merge(
            routing_key: routing_key,
            queue: queue.name,
          )
        end

        def sanitaze_routing_key(routing_key)
          return if routing_key.nil?

          @routing_key_sanitazer.sanitaze_routing_key(routing_key)
        end
      end
    end
  end
end
