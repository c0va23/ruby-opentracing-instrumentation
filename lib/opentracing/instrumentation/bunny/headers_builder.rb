# frozen_string_literal: true

module OpenTracing
  module Instrumentation
    module Bunny
      # HeadersBuilder build AMQP headers
      class HeadersBuilder
        # @param tracer [OpenTracing::Tracer]
        # @param injector [HeadersInjector]
        def initialize(
          tracer: OpenTracing.global_tracer,
          injector: HeadersInjector.new(tracer: tracer)
        )
          @tracer = tracer
          @injector = injector
        end

        # @param active_span [OpenTracing::Span]
        # @return [Hash<String, String>] return injected headers
        def build(active_span: @tracer.active_span)
          headers = {}
          @injector.inject(headers, active_span: active_span)
          headers
        end
      end
    end
  end
end
