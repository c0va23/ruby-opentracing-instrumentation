# frozen_string_literal: true

module OpenTracing
  module Instrumentation
    module Bunny
      # PublishTracerConfig for PublishTracer
      class PublishTracerConfig
        # @return [OpenTracing::Tracer]
        attr_accessor :tracer
        # @return [PublishOperationNameBuilder]
        attr_accessor :operation_name_builder
        # @return [PublishTagsBuilder]
        attr_accessor :tags_builder
        # @return [HeadersInjector]
        attr_accessor :injector
        # @return [Common::ErrorWriter]
        attr_accessor :error_writer
        # @return [::Logger]
        attr_accessor :logger

        def initialize
          @tracer = OpenTracing.global_tracer
          @operation_name_builder = PublishOperationNameBuilder.new
          @tags_builder = PublishTagsBuilder.new
          @injector = HeadersInjector.new
          @error_writer = Common::ErrorWriter.new
        end
      end
    end
  end
end
