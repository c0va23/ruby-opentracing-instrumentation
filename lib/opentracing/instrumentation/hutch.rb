# frozen_string_literal: true

module OpenTracing
  module Instrumentation
    # Module Hutch allow tracing Hutch consuming and inject
    # tracing headers on publishing
    #
    # @see ConsumeTracer
    # @see ConsumeTracerBuilder
    # @see GlobalPropertiesBuilder
    module Hutch
      module_path = 'opentracing/instrumentation/hutch'

      autoload :ConsumeOperationNameBuilder,
               "#{module_path}/consume_operation_name_builder"
      autoload :ConsumeTagsBuilder,
               "#{module_path}/consume_tags_builder"
      autoload :ConsumeTracer,
               "#{module_path}/consume_tracer"
      autoload :ConsumeTracerBuilder,
               "#{module_path}/consume_tracer_builder"
      autoload :ConsumeTracerConfig,
               "#{module_path}/consume_tracer_config"
      autoload :GlobalPropertiesBuilder,
               "#{module_path}/global_properties_builder"
    end
  end
end
