# frozen_string_literal: true

require 'json'

module OpenTracing
  module Instrumentation
    # Common classes
    module Common
      autoload :ErrorWriter,
               'opentracing/instrumentation/common/error_writer'
      autoload :OperationNameBuilder,
               'opentracing/instrumentation/common/operation_name_builder'
    end
  end
end
