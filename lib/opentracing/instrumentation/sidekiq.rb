# frozen_string_literal: true

module OpenTracing
  module Instrumentation
    # Sidekiq instrumentation
    module Sidekiq
      autoload :ClientMiddleware,
               'opentracing/instrumentation/sidekiq/client_middleware'

      autoload :JobTagger,
               'opentracing/instrumentation/sidekiq/job_tagger'

      autoload :ServerMiddleware,
               'opentracing/instrumentation/sidekiq/server_middleware'
    end
  end
end
