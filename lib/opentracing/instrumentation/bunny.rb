# frozen_string_literal: true

module OpenTracing
  module Instrumentation
    # Bunny tracing instrumentation
    #
    # @see ConsumeTracer
    # @see PublishTracer
    module Bunny
      module_path = 'opentracing/instrumentation/bunny'

      autoload :ConsumeOperationNameBuilder,
               "#{module_path}/consume_operation_name_builder"
      autoload :ConsumeTagsBuilder,
               "#{module_path}/consume_tags_builder"
      autoload :ConsumeTracer,
               "#{module_path}/consume_tracer"
      autoload :ConsumeTracerConfig,
               "#{module_path}/consume_tracer_config"
      autoload :HeadersBuilder,
               "#{module_path}/headers_builder"
      autoload :HeadersInjector,
               "#{module_path}/headers_injector"
      autoload :PublishOperationNameBuilder,
               "#{module_path}/publish_operation_name_builder"
      autoload :PublishTagsBuilder,
               "#{module_path}/publish_tags_builder"
      autoload :PublishTracer,
               "#{module_path}/publish_tracer"
      autoload :PublishTracerConfig,
               "#{module_path}/publish_tracer_config"
      autoload :RegexpRoutingKeySanitazer,
               "#{module_path}/regexp_routing_key_sanitazer"
    end
  end
end
