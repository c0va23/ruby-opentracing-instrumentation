# frozen_string_literal: true

module OpenTracing
  module Instrumentation
    module Mongo
      # DirectSanitazer requests.
      class DirectSanitazer
        def sanitaze(command, command_name)
          command = command.dup
          command.delete(command_name)
          command
        end
      end
    end
  end
end
