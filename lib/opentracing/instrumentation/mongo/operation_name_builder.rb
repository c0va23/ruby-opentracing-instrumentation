# frozen_string_literal: true

module OpenTracing
  module Instrumentation
    module Mongo
      # OperationNameBuilder for Mongo::TraceSubscriber
      class OperationNameBuilder
        DEFAULT_OPERATION_NAME_PATTERN = \
          'mongo(collection=%<database>s.%<collection>s, command=%<command>s)'

        # @param operation_name_pattern [String]
        def initialize(
          operation_name_pattern: DEFAULT_OPERATION_NAME_PATTERN
        )
          @operation_name_pattern = operation_name_pattern
        end

        # @param event [Mongo::Monitoring::Event::CommandStarted]
        # @return [String] formated command name
        def build_operation_name(event)
          format_args = build_format_args(event)
          format(operation_name_pattern, **format_args)
        end

        private

        attr_reader :operation_name_pattern

        COLLECTION = 'collection'

        def build_format_args(event)
          {
            database: event.database_name,
            collection: fetch_collection_name(event),
            command: event.command_name,
          }
        end

        def fetch_collection_name(event)
          # On some command collection name into 'collection' field,
          event.command[COLLECTION] || event.command[event.command_name]
        end
      end
    end
  end
end
