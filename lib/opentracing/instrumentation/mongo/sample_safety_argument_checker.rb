# frozen_string_literal: true

module OpenTracing
  module Instrumentation
    module Mongo
      # SampleSafetyArgumentChecker check argument to safety
      # Draft implementation
      class SampleSafetyArgumentChecker
        DEFAULT_SAFE_ARGUMENTS = [
          '$readPreference',
          '$clusterTime',
        ].freeze

        attr_reader :safe_arguments

        def initialize(safe_arguments: DEFAULT_SAFE_ARGUMENTS)
          @safe_arguments = safe_arguments
        end

        # check
        #
        # @return (TrueClass, FalseClass) `true`, if argument safe and not
        # not should be cleaned. Otherwise return `false``.
        def argument_safe?(_command_name, argument_path, _argument_value)
          safe_arguments.include?(argument_path)
        end
      end
    end
  end
end
