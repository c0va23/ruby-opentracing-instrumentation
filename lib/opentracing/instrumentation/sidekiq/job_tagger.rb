# frozen_string_literal: true

require 'json'

module OpenTracing
  module Instrumentation
    module Sidekiq
      # Span tags and logs building mixin
      class JobTagger
        LOG_JID_EVENT = 'job_id'
        LOG_ARGS_EVENT = 'job_args'

        DEFAULT_COMPONENT = 'sidekiq'

        attr_accessor :component,
                      :log_args

        # @param component [String] component name
        # @param log_args [TrueClass, FalseClass] enable attribute logging
        def initialize(
          component: DEFAULT_COMPONENT,
          log_args: false
        )
          @component = component
          @log_args = log_args

          yield self if block_given?
        end

        # build tags from job data and static attributes
        def build_tags(job, span_kind)
          {
            component: component,
            'span.kind': span_kind,
            'sidekiq.queue': job['queue'],
            'sidekiq.class': job['class'],
            'sidekiq.retry': job['retry'],
          }
        end

        # write job jid and args if log_args enabled
        # @param span [OpenTracing::Span] target span
        # @param jid [String] job id
        # @param args [Array<Object>] argument list
        def write_args_log(span, jid, args)
          span.log_kv(
            event: LOG_JID_EVENT,
            jid: jid,
          )

          return unless log_args

          span.log_kv(
            event: LOG_ARGS_EVENT,
            args: JSON.dump(args),
          )
        end
      end
    end
  end
end
