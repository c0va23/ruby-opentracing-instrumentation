# frozen_string_literal: true

require 'json'

module OpenTracing
  module Instrumentation
    module Sidekiq
      # Sidekiq server middleware
      class ServerMiddleware
        extend Forwardable

        DEFAULT_SPAN_KIND = 'consumer'

        DEFAULT_OPERATION_NAME_TEMPLATE = \
          'sidekiq_perform(%<sidekiq.class>s)'
        DEFAULT_OPERATION_NAME_BUILDER = \
          Common::OperationNameBuilder.new(
            operation_name_template: DEFAULT_OPERATION_NAME_TEMPLATE,
          )

        attr_reader :tracer,
                    :tagger,
                    :error_writter,
                    :logger,
                    :span_kind,
                    :operation_name_builder

        # rubocop:disable Metrics/ParameterLists
        def initialize(
          tracer: OpenTracing.global_tracer,
          tagger: JobTagger.new,
          error_writter: Common::ErrorWriter.new,
          logger: nil,
          span_kind: DEFAULT_SPAN_KIND,
          operation_name_builder: DEFAULT_OPERATION_NAME_BUILDER
        )
          @tracer = tracer
          @tagger = tagger
          @error_writter = error_writter
          @logger = logger
          @span_kind = span_kind
          @operation_name_builder = operation_name_builder
        end
        # rubocop:enable Metrics/ParameterLists

        def call(_worker, job, _queue, &block)
          scope = safe_start_scope(job)

          log(scope.span, job, &block)
        ensure
          scope&.close
        end

        private

        def safe_start_scope(job)
          tags =  tagger.build_tags(job, span_kind)
          operation_name = operation_name_builder.build(tags)
          tracer.start_active_span(operation_name, tags: tags, **build_span_args(job))
        rescue StandardError => e
          logger&.error(e)
        end

        def safe_close_scope(scope)
          return unless socpe

          scope.close
        rescue StandardError => e
          logger&.error(e)
        end

        def log(span, job)
          tagger.write_args_log(span, job['jid'], job['args'])

          yield
        rescue StandardError => e
          error_writter.write_error(span, e)
          raise
        end

        def build_span_args(job)
          {
            references: extract_references(job),
          }
        end

        def extract_references(job)
          span_context = tracer.extract(OpenTracing::FORMAT_TEXT_MAP, job)
          return unless span_context

          [OpenTracing::Reference.follows_from(span_context)]
        end
      end
    end
  end
end
