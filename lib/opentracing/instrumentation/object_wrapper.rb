# frozen_string_literal: true

module OpenTracing
  module Instrumentation
    # ObjectWrapper allow trace call methods on any ruby object.
    #
    # Usage:
    #   wrapped_object = OpenTracing::Instrumentation::ObjectWrapper.new(other_object)
    #   wrapped_object.other_object_method
    class ObjectWrapper
      DEFAULT_OPERATOIN_NAME_PATTERN = \
        'object_call(%<class_name>s#%<method>s)'

      attr_reader :tracer,
                  :operation_name_pattern

      def initialize(
        object,
        tracer: OpenTracing.global_tracer,
        operation_name_pattern: DEFAULT_OPERATOIN_NAME_PATTERN
      )
        @object = object
        @tracer = tracer
        @operation_name_pattern = operation_name_pattern
      end

      # wrapped object
      attr_reader :object

      def respond_to_missing?(method_name, *)
        object.respond_to?(method_name)
      end

      def method_missing(method_name, *args)
        return super unless object.respond_to?(method_name)

        operation_name = buid_operation_name(method_name)
        tags = build_tags(method_name)
        tracer.start_active_span(operation_name, tags: tags) do |scope|
          call_method(scope.span, method_name, *args)
        end
      end

      private

      def buid_operation_name(method_name)
        format(
          operation_name_pattern,
          class_name: class_name,
          method: method_name,
        )
      end

      def call_method(span, method_name, *args)
        object.send(method_name, *args)
      rescue StandardError => e
        span.set_tag('error', true)
        raise e
      end

      def class_name
        @class_name ||= object.class.to_s
      end

      def build_tags(method)
        {
          'object.class' => class_name,
          'object.method' => method.to_s,
        }
      end
    end
  end
end
