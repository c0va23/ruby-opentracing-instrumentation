# frozen_string_literal: true

module OpenTracing
  module Instrumentation
    VERSION = File.read(File.expand_path('../../../GEM_VERSION', __dir__)).strip
  end
end
