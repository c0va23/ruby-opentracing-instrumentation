# frozen_string_literal: true

module OpenTracing
  module Instrumentation
    # Redis tracing drivers
    module Redis
      EVENT_WRITE = 'redis_write'
      EVENT_READ = 'redis_read'

      autoload :Config,
               'opentracing/instrumentation/redis/config'
      autoload :SpanBuilder,
               'opentracing/instrumentation/redis/span_builder'
      autoload :TracingDriverWrapper,
               'opentracing/instrumentation/redis/tracing_driver_wrapper'
    end
  end
end
