# frozen_string_literal: true

require 'sinatra/base'

module OpenTracing
  module Instrumentation
    module Sinatra
      # TraceMiddleware for sinatra
      #
      # Usage
      #   class MyApp < Sinatra::Base
      #     use OpenTracing::Instrumentation::Sinatra::TraceMiddleware
      #     set :tracer, MyTracer.instance # optionaly
      #   end
      module TraceMiddleware
        def self.registered(app)
          app.helpers self

          app.set :tracer, OpenTracing.global_tracer
          app.set :command_name, 'sinatra_request'

          app.before do
            start_span
          end

          app.after do
            close_span
          end
        end

        private

        def start_span
          @scope = settings.tracer.start_active_span(
            settings.command_name,
            tags: build_base_tags,
          )
        end

        def close_span
          set_response_span_tags(@scope.span, env)
          @scope.close
        end

        def build_base_tags
          {
            'sinatra.app' => self.class.to_s,
            'sinatra.environment' => settings.environment,
            'sinatra.threaded' => settings.threaded,
            'sinatra.show_exceptions' => settings.show_exceptions,
          }
        end

        def set_response_span_tags(span, env)
          sinatra_route = env['sinatra.route']
          span.set_tag 'sinatra.route', sinatra_route

          sinatra_error = env['sinatra.error']
          span.set_tag('error', true) if sinatra_error
        end
      end
    end
  end
end
