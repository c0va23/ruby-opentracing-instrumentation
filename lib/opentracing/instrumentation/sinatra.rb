# frozen_string_literal: true

module OpenTracing
  module Instrumentation
    # OpenTracing instrumentation for Sinatra
    module Sinatra
      autoload :TraceMiddleware,
               'opentracing/instrumentation/sinatra/trace_middleware'
    end
  end
end
