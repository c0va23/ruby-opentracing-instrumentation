# frozen_string_literal: true

module OpenTracing
  module Instrumentation
    module Faraday
      # ResponseLogger add request and response log_kv to faraday_request span
      class ResponseLogger
        DEFAULT_REQUEST_LOG_HEADERS = %w[].freeze
        DEFAULT_REQUEST_TAG_HEADERS = {
          accept: 'Accept',
          connection: 'Connection',
          content_type: 'Content-Type',
          user_agent: 'User-Agent',
        }.freeze
        DEFAULT_RESPONSE_LOG_HEADERS = %w[
          Content-Length
        ].freeze
        DEFAULT_RESPONSE_TAG_HEADERS = {
          connection: 'Connection',
          content_type: 'Content-Type',
        }.freeze

        attr_reader :request_tag_headers,
                    :request_log_headers,
                    :response_tag_headers,
                    :response_log_headers

        def initialize(
          request_log_headers: DEFAULT_REQUEST_LOG_HEADERS,
          request_tag_headers: DEFAULT_REQUEST_TAG_HEADERS,
          response_tag_headers: DEFAULT_RESPONSE_TAG_HEADERS,
          response_log_headers: DEFAULT_RESPONSE_LOG_HEADERS
        )
          @request_tag_headers = request_tag_headers
          @request_log_headers = request_log_headers
          @response_tag_headers = response_tag_headers
          @response_log_headers = response_log_headers
        end

        def log_request(span, request_headers)
          log_headers(span, request_headers, request_log_headers, 'request')
          tag_headers(span, request_headers, request_tag_headers, 'request')
        end

        def log_response(span, response_headers)
          log_headers(span, response_headers, response_log_headers, 'response')
          tag_headers(span, response_headers, response_tag_headers, 'response')
        end

        private

        def tag_headers(span, headers, tag_headers, event)
          tag_headers.each do |key, header_name|
            header_value = headers[header_name]
            next unless header_value

            span.set_tag("http.#{event}.#{key}", header_value)
          end
        end

        def log_headers(span, headers, log_headers, event)
          header_hash =
            log_headers
            .map { |key| [key, headers[key]] }
            .reject { |(_key, value)| value.nil? }
            .to_h

          span.log_kv(
            event: event,
            headers: JSON.dump(header_hash),
          )
        end
      end
    end
  end
end
