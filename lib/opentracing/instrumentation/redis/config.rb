# frozen_string_literal: true

require 'redis/connection/ruby'

module OpenTracing
  module Instrumentation
    module Redis
      # Redis tracing mixin config
      class Config
        DEFAULT_OPERATION_NAME_PATTERN = \
          'redis(command=%<command>s)'
        # Safe by default
        DEFAULT_LOG_ARGS = false
        DEFAULT_LOG_REPLY = false
        DEFAULT_COMPONENT = 'kv'

        attr_accessor :tracer,
                      :operation_name_pattern,
                      :log_args,
                      :log_reply,
                      :component

        def initialize(
          tracer: OpenTracing.global_tracer,
          operation_name_pattern: DEFAULT_OPERATION_NAME_PATTERN,
          log_args: DEFAULT_LOG_ARGS,
          log_reply: DEFAULT_LOG_REPLY,
          component: DEFAULT_COMPONENT
        )
          @tracer = tracer
          @operation_name_pattern = operation_name_pattern
          @log_args = log_args
          @log_reply = log_reply
          @component = component

          yield self if block_given?
        end
      end
    end
  end
end
