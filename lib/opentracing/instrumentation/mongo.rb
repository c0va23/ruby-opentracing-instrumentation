# frozen_string_literal: true

module OpenTracing
  module Instrumentation
    # Mongo driver instrumentation. Support mongo gem version 2.x.
    module Mongo
      autoload :DirectSanitazer, 'opentracing/instrumentation/mongo/direct_sanitazer'
      autoload :OperationNameBuilder,
               'opentracing/instrumentation/mongo/operation_name_builder'
      autoload :TraceSubscriber, 'opentracing/instrumentation/mongo/trace_subscriber'
      autoload :QuerySanitazer, 'opentracing/instrumentation/mongo/query_sanitazer'
      autoload :SampleSafetyArgumentChecker,
               'opentracing/instrumentation/mongo/sample_safety_argument_checker'
    end
  end
end
