# frozen_string_literal: true

require 'opentracing/instrumentation/version'

module OpenTracing
  # module OpenTracing::Instrumentation provide instrumentation modules for some
  # popular Ruby gems.
  #
  # All instrumentations modules is lazy loaded (via autoload).
  #
  # {file:README.md}
  #
  # @see Bunny
  # @see Faraday
  # @see Hutch
  # @see Mongo
  # @see Rack
  # @see Redis
  # @see Sidekiq
  # @see Sinatra
  # @see Thrift
  module Instrumentation
    class Error < StandardError; end

    autoload :Bunny, 'opentracing/instrumentation/bunny'
    autoload :Common, 'opentracing/instrumentation/common'
    autoload :Faraday, 'opentracing/instrumentation/faraday'
    autoload :Hutch, 'opentracing/instrumentation/hutch'
    autoload :Mongo, 'opentracing/instrumentation/mongo'
    autoload :ObjectWrapper, 'opentracing/instrumentation/object_wrapper'
    autoload :Rack, 'opentracing/instrumentation/rack'
    autoload :Redis, 'opentracing/instrumentation/redis'
    autoload :Sidekiq, 'opentracing/instrumentation/sidekiq'
    autoload :Sinatra, 'opentracing/instrumentation/sinatra'
    autoload :Thrift, 'opentracing/instrumentation/thrift'
  end
end
