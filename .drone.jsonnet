local TestPipeline(os, arch, ruby_version) = {
  kind: "pipeline",
  type: "kubernetes",
  name: os + "_" + arch + "_ruby_" + ruby_version,

  platform: {
    os: os,
    arch: arch,
  },

  steps: [
    {
      name: "tests",
      image: "ruby:" + ruby_version + "-alpine",
      commands: [
        "apk add --update git ruby-dev build-base",
        "gem install bundler:$$(cat .BUNDLER_VERSION)",
        "bundle",
        "rake",
      ]
    }
  ]
};

local TARGETS = [
  {os: "linux", arch: "arm", ruby_version: "2.5"},
  {os: "linux", arch: "arm", ruby_version: "2.6"},
  {os: "linux", arch: "arm64", ruby_version: "2.7"},
  {os: "linux", arch: "arm64", ruby_version: "3.0"},
];

[
  TestPipeline(target.os, target.arch, target.ruby_version)
  for target in TARGETS
]
